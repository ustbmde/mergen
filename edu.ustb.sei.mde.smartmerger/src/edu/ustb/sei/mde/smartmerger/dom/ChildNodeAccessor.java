package edu.ustb.sei.mde.smartmerger.dom;

import java.util.List;

public class ChildNodeAccessor extends PathNode {
	public ChildNodeAccessor(int position) {
		super();
		this.position = position;
	}

	protected int position;

	@Override
	protected Object internalGet(Object current) {
		if(current instanceof List) {
			if(position < ((List<?>) current).size())
				return ((List<?>) current).get(position);
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void internalSet(Object current, Object value) {
		if(current instanceof List) {
			if(position < ((List<?>) current).size())
				((List) current).set(position, value);
		}
	}

}
