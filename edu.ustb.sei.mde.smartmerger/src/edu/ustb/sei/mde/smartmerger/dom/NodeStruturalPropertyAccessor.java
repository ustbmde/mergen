package edu.ustb.sei.mde.smartmerger.dom;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ChildListPropertyDescriptor;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;

public class NodeStruturalPropertyAccessor extends PathNode {
	protected StructuralPropertyDescriptor descriptor;

	public StructuralPropertyDescriptor getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(StructuralPropertyDescriptor descriptor) {
		this.descriptor = descriptor;
	}

	@Override
	protected Object internalGet(Object current) {
		if(current instanceof ASTNode) {
			return ((ASTNode) current).getStructuralProperty(descriptor);
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void internalSet(Object current, Object value) {
		if(current instanceof ASTNode) {
			if(descriptor instanceof ChildListPropertyDescriptor) {
				List<?> children = (List<?>) ((ASTNode) current).getStructuralProperty(descriptor);
				children.clear();
				children.addAll((List)value);
			} else {				
				((ASTNode) current).setStructuralProperty(descriptor, value);
			}
		}
	}

}
