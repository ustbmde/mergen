package edu.ustb.sei.mde.smartmerger.dom;

import org.eclipse.jdt.core.dom.ASTNode;

public class ParentAccessor extends PathNode {

	@Override
	protected Object internalGet(Object current) {
		if(current instanceof ASTNode) {
			return ((ASTNode) current).getParent();
		}
		return null;
	}

	@Override
	protected void internalSet(Object current, Object value) {
		throw new UnsupportedOperationException();
	}
}
