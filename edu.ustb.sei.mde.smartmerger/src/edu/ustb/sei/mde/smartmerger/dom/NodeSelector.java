package edu.ustb.sei.mde.smartmerger.dom;

import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;

import org.eclipse.jdt.core.dom.ASTNode;

public class NodeSelector extends PathNode {
	public NodeSelector(Class<?> type, PropertySelectorEntry... propertySelectors) {
		this(type, Arrays.asList(propertySelectors));
	}
	
	public NodeSelector(Class<?> type, List<Entry<PathNode, Object>> propertySelectors) {
		super();
		this.type = type;
		this.propertySelectors = propertySelectors;
	}

	protected Class<?> type;
	protected List<Entry<PathNode, Object>> propertySelectors;
	
	static public class PropertySelectorEntry implements Entry<PathNode, Object> {
		public PropertySelectorEntry(PathNode pathKey, Object value) {
			super();
			this.pathKey = pathKey;
			this.value = value;
		}

		private PathNode pathKey;
		private Object value;

		@Override
		public PathNode getKey() {
			return pathKey;
		}

		@Override
		public Object getValue() {
			return value;
		}

		@Override
		public Object setValue(Object value) {
			Object oldValue = this.value;
			this.value = value;
			return oldValue;
		}

	}

	@Override
	protected Object internalGet(Object current) {
		if(current instanceof ASTNode) {
			if(type==null || current.getClass()==type) {				
				if(propertySelectors.stream().allMatch(sel->{
					Object value = sel.getKey().get(current);
					return Objects.equals(value, sel.getValue());
				})) {
					return current;
				}
			}
		} else if(current instanceof List) {
			return ((List<?>) current).stream().filter(node->{
				return internalGet(node)!=null;
			}).findFirst().orElse(null);
		} else {
			if(propertySelectors.stream().allMatch(sel->{
				return Objects.equals(current, sel.getValue());
			})) {
				return current;
			}
		}
		return null;
	}

	@Override
	protected void internalSet(Object current, Object value) {
		throw new UnsupportedOperationException();
	}
}
