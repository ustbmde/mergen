package edu.ustb.sei.mde.smartmerger.dom;

public class CombinedPathNode extends PathNode {
	protected PathNode first;
	protected PathNode second;
	@Override
	protected Object internalGet(Object current) {
		Object firstGet = first.get(current);
		if(firstGet!=null) return second.get(firstGet);
		return null;
	}
	@Override
	protected void internalSet(Object current, Object value) {
		Object firstGet = first.get(current);
		if(firstGet!=null) second.set(firstGet, value);
	}
	
	
}
