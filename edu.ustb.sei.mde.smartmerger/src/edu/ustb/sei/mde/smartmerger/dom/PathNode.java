package edu.ustb.sei.mde.smartmerger.dom;

public abstract class PathNode {

	public PathNode() {
		next = null;
	}
	
	protected PathNode next;
	
	public Object get(Object current) {
		Object internalResult = internalGet(current);
		if(internalResult!=null && next!=null) {
			return next.get(internalResult);
		}
		return null;
	}
	
	abstract protected Object internalGet(Object current);
	
	public void set(Object current, Object value) {
		if(next==null) {
			internalSet(current, value);
		} else {
			Object internalResult = internalGet(current);
			if(internalResult!=null) {
				next.set(internalResult, value);
			}
		}
	}
	
	abstract protected void internalSet(Object current, Object value);
}
