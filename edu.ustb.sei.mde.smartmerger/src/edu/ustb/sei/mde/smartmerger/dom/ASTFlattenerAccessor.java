package edu.ustb.sei.mde.smartmerger.dom;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.internal.corext.dom.ASTFlattener;

@SuppressWarnings("restriction")
public class ASTFlattenerAccessor extends PathNode {
	public ASTFlattenerAccessor() {
		super();
	}
	
	public ASTFlattenerAccessor(String separator) {
		super();
		this.separator = separator;
	}

	static protected ASTFlattener flattener = new ASTFlattener();
	protected String separator = ",";

	@SuppressWarnings("unchecked")
	@Override
	protected Object internalGet(Object current) {
		if(current instanceof ASTNode) {
			flattener.reset();
			((ASTNode) current).accept(flattener);
			String text = flattener.getResult();
			return text;
		} else if(current instanceof List) {
			List<ASTNode> list = (List<ASTNode>) current;
			if(list.isEmpty()) return "";
			else if(list.size()==1) return internalGet(list.get(0));
			else {
				StringBuilder builder = new StringBuilder();
				for(int i=0;i<list.size();i++) {
					if(i!=0) builder.append(",");
					builder.append(internalGet(list.get(i)));
				}
				return builder.toString();
			}
		}
		return null;
	}

	@Override
	protected void internalSet(Object current, Object value) {
		if(current instanceof SimpleName) {
			((SimpleName)current).setIdentifier(value.toString());
		} else {
			throw new UnsupportedOperationException();
		}
	}

}
