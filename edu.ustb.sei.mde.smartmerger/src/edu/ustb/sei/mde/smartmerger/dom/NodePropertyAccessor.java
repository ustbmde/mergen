package edu.ustb.sei.mde.smartmerger.dom;

import org.eclipse.jdt.core.dom.ASTNode;

public class NodePropertyAccessor extends PathNode {
	public NodePropertyAccessor(String property) {
		super();
		this.property = property;
	}

	protected String property;

	@Override
	protected Object internalGet(Object current) {
		if(current instanceof ASTNode) {
			return ((ASTNode) current).getProperty(property);
		}
		return null;
	}

	@Override
	protected void internalSet(Object current, Object value) {
		if(current instanceof ASTNode) {
			((ASTNode) current).setProperty(property, value);
		}
	}

}
