package edu.ustb.sei.mde.smartmerger.java;

import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.CompilationUnit;

public class CodeBase {
	public Map<ICompilationUnit, CompilationUnit> compilationUnitMap;
	public CodeIndexBase indexBase;
	public IJavaProject project;
	
	static public CodeBase build(IJavaProject project, TagPatternDictionary dict) {
		CodeBase base = new CodeBase();
		base.project = project;
		List<ICompilationUnit> units = JavaModelHelper.getAllCompilationUnits(project);
		base.compilationUnitMap = JavaModelHelper.collectASTs(project, units);
		CodeIndexBase index = new CodeIndexBase();
		JavaModelHelper.extractASTInformation(dict, base.compilationUnitMap, index);
		base.indexBase = index;
		return base;
	}
}
