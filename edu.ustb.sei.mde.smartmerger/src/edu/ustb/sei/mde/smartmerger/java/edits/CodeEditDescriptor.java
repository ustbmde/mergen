package edu.ustb.sei.mde.smartmerger.java.edits;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ChildListPropertyDescriptor;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;

import edu.ustb.sei.mde.smartmerger.java.TagInfoPattern;

public class CodeEditDescriptor {
	public ASTNode nodeInSource;
	public ASTNode nodeInView;
	public StructuralPropertyDescriptor nodeProperty;
	public Object propertyValue;
	public EditKind kind;
	public TagInfoPattern tagPattern;
	
	static public CodeEditDescriptor createDescriptor(ASTNode nodeInSource, ASTNode nodeInView, StructuralPropertyDescriptor nodeProperty, Object propertyValue, EditKind kind, TagInfoPattern tagPattern) {
		CodeEditDescriptor descriptor = new CodeEditDescriptor();
		descriptor.nodeInSource = nodeInSource;
		descriptor.nodeInView = nodeInView;
		descriptor.nodeProperty = nodeProperty;
		descriptor.propertyValue = propertyValue;
		descriptor.kind = kind;
		descriptor.tagPattern = tagPattern;
		return descriptor;
	}
	
	static public CodeEditDescriptor createRemoveDescriptor(ASTNode nodeInSource, TagInfoPattern tagPattern) {
		return createDescriptor(nodeInSource, null, null, null, EditKind.REMOVE, tagPattern);
	}
	
	static public CodeEditDescriptor createPushAllDescriptor(ASTNode nodeInSource, ASTNode nodeInView, StructuralPropertyDescriptor nodeProperty, TagInfoPattern tagPattern) {
		if(nodeProperty instanceof ChildListPropertyDescriptor) {
			return createDescriptor(nodeInSource, nodeInView, nodeProperty, null, EditKind.PUSH_ALL, tagPattern);
		} else throw new RuntimeException();
	}
	
	static public CodeEditDescriptor createPullAllDescriptor(ASTNode nodeInSource, ASTNode nodeInView, StructuralPropertyDescriptor nodeProperty, TagInfoPattern tagPattern) {
		if(nodeProperty instanceof ChildListPropertyDescriptor) {
			return createDescriptor(nodeInSource, nodeInView, nodeProperty, null, EditKind.PULL_ALL, tagPattern);
		} else throw new RuntimeException();
	}
}
