package edu.ustb.sei.mde.smartmerger.java;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import edu.ustb.sei.mde.infrastructure.util.MultiMap;

public class TagPatternDictionary {
	private static final String SMART_MERGER_IS_GENERATED = "SMART_MERGER__IS_GENERATED";
	private static final String SMART_MERGER_UUID = "SMART_MERGER__UUID";
	
	private static final String SMART_MERGER_BODY_HASH = "SMART_MERGER__BODY_HASH";
	private static final String SMART_MERGER_MODIFIER_HASH = "SMART_MERGER__MODIFIER_HASH";
	private static final String SMART_MERGER_TYPE_HASH = "SMART_MERGER__TYPE_HASH";
	private static final String SMART_MERGER_TYPE_PARAMETERS_HASH = "SMART_MERGER__TYPE_PARAMETERS_HASH";
	private static final String SMART_MERGER_SUPER_INTERFACES_HASH = "SMART_MERGER__SUPER_INTERFACES_HASH";
	private static final String SMART_MERGER_SUPER_TYPE_HASH = "SMART_MERGER__SUPER_TYPE_HASH";
	private static final String SMART_MERGER_NAME_HASH = "SMART_MERGER__NAME_HASH";
	private static final String SMART_MERGER_INITIALIZER_HASH = "SMART_MERGER__INITIALIZER_HASH";
	private static final String SMART_MERGER_PARAMETERS_HASH = "SMART_MERGER__PARAMETERS_HASH";
	private static final String SMART_MERGER_THROWN_EXCEPTION_TYPES_HASH = "SMART_MERGER__THROWN_EXCEPTION_TYPES_HASH";
	
	
	private MultiMap<Class<?>, TagInfoPattern> patternMap = new MultiMap<Class<?>, TagInfoPattern>();
	private Set<String> tagNames = new HashSet<>();
	
	public Collection<TagInfoPattern> getPatterns(Class<?> clazz) {
		return patternMap.get(clazz);
	}
	
	static public final TagInfoPattern isGenerated = new TagInfoPattern(SMART_MERGER_IS_GENERATED, "@generated", TagInfoKind.EXISTENCE, null);
	static public final TagInfoPattern uuid = new TagInfoPattern(SMART_MERGER_UUID, "@uuid", TagInfoKind.STRING, null);
	
	static public final TagInfoPattern nameHash = new TagInfoPattern(SMART_MERGER_NAME_HASH, "@name_hash", TagInfoKind.HASH, "0", Map.of(TypeDeclaration.class, TypeDeclaration.NAME_PROPERTY, EnumDeclaration.class, EnumDeclaration.NAME_PROPERTY, FieldDeclaration.class, VariableDeclarationFragment.NAME_PROPERTY, MethodDeclaration.class, MethodDeclaration.NAME_PROPERTY));
	static public final TagInfoPattern superTypeHash = new TagInfoPattern(SMART_MERGER_SUPER_TYPE_HASH, "@super_type_hash", TagInfoKind.HASH, "0", Map.of(TypeDeclaration.class, TypeDeclaration.SUPERCLASS_TYPE_PROPERTY));
	static public final TagInfoPattern superInterfacesHash = new TagInfoPattern(SMART_MERGER_SUPER_INTERFACES_HASH, "@super_interfaces_hash", TagInfoKind.HASH, "0", Map.of(TypeDeclaration.class, TypeDeclaration.SUPER_INTERFACE_TYPES_PROPERTY));
	static public final TagInfoPattern typeHash = new TagInfoPattern(SMART_MERGER_TYPE_HASH, "@type_hash", TagInfoKind.HASH, "0", Map.of(FieldDeclaration.class, FieldDeclaration.TYPE_PROPERTY, MethodDeclaration.class, MethodDeclaration.RETURN_TYPE2_PROPERTY));
	static public final TagInfoPattern bodyHash = new TagInfoPattern(SMART_MERGER_BODY_HASH, "@body_hash", TagInfoKind.HASH, "0", Map.of(MethodDeclaration.class, MethodDeclaration.BODY_PROPERTY));
	static public final TagInfoPattern modifierHash = new TagInfoPattern(SMART_MERGER_MODIFIER_HASH, "@modifier_hash", TagInfoKind.HASH, "0",  Map.of(TypeDeclaration.class, TypeDeclaration.MODIFIERS2_PROPERTY, EnumDeclaration.class, EnumDeclaration.MODIFIERS2_PROPERTY, FieldDeclaration.class, FieldDeclaration.MODIFIERS2_PROPERTY, MethodDeclaration.class, MethodDeclaration.MODIFIERS2_PROPERTY));
	static public final TagInfoPattern typeParameterHash = new TagInfoPattern(SMART_MERGER_TYPE_PARAMETERS_HASH, "@type_parameters_hash", TagInfoKind.HASH, "0", Map.of(TypeDeclaration.class, TypeDeclaration.TYPE_PARAMETERS_PROPERTY, MethodDeclaration.class, MethodDeclaration.TYPE_PARAMETERS_PROPERTY));
	static public final TagInfoPattern initalizerHash = new TagInfoPattern(SMART_MERGER_INITIALIZER_HASH, "@initializer_hash", TagInfoKind.HASH, "0", Map.of(FieldDeclaration.class, VariableDeclarationFragment.INITIALIZER_PROPERTY));
	static public final TagInfoPattern parametersHash = new TagInfoPattern(SMART_MERGER_PARAMETERS_HASH, "@parameters_hash", TagInfoKind.HASH, "0", Map.of(MethodDeclaration.class, MethodDeclaration.PARAMETERS_PROPERTY));
	static public final TagInfoPattern throwExceptionTypesHash = new TagInfoPattern(SMART_MERGER_THROWN_EXCEPTION_TYPES_HASH, "@throw_exception_types_hash", TagInfoKind.HASH, "0", Map.of(MethodDeclaration.class, MethodDeclaration.THROWN_EXCEPTION_TYPES_PROPERTY));
	
	
	public TagPatternDictionary() {
		// initialize the map
		buildDefaultTypePatterns();	
		buildDefaultFieldPatterns();
		buildDefaultMethodPatterns();
	}

	public void addPattern(Class<?> type, TagInfoPattern pattern) {
		this.patternMap.put(type, pattern);
		tagNames.add(pattern.tagName);
	}

	private void buildDefaultTypePatterns() {
		buildDefaultPatterns(TypeDeclaration.class);
		addPattern(TypeDeclaration.class, superTypeHash);
		addPattern(TypeDeclaration.class, superInterfacesHash);
		addPattern(TypeDeclaration.class, typeParameterHash);
	}
	
	private void buildDefaultFieldPatterns() {
		buildDefaultPatterns(FieldDeclaration.class);
		addPattern(FieldDeclaration.class, typeHash);
		addPattern(FieldDeclaration.class, initalizerHash);
	}
	
	private void buildDefaultMethodPatterns() {
		buildDefaultPatterns(MethodDeclaration.class);
		addPattern(MethodDeclaration.class, typeHash);
		addPattern(MethodDeclaration.class, typeParameterHash);
		addPattern(MethodDeclaration.class, parametersHash);
		addPattern(MethodDeclaration.class, throwExceptionTypesHash);
		addPattern(MethodDeclaration.class, bodyHash);
		
	}
	
	public Stream<TagInfoPattern> getHashPatterns(Class<?> clazz) {
		return patternMap.get(clazz).stream().filter(p->p.kind==TagInfoKind.HASH);
	}




	private void buildDefaultPatterns(Class<?> clazz) {
		addPattern(clazz, isGenerated);
		addPattern(clazz, uuid);
		
		addPattern(clazz, nameHash);
		addPattern(clazz, modifierHash);
	}
	
	public boolean isTag(String key) {
		return tagNames.contains(key);
	}
}