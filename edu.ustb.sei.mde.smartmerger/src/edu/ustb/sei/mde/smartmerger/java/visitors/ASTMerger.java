package edu.ustb.sei.mde.smartmerger.java.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTMatcher;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.ChildListPropertyDescriptor;
import org.eclipse.jdt.core.dom.ChildPropertyDescriptor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IDocElement;
import org.eclipse.jdt.core.dom.IExtendedModifier;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimplePropertyDescriptor;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;
import org.eclipse.jdt.core.refactoring.IJavaRefactorings;
import org.eclipse.jdt.core.refactoring.descriptors.RenameJavaElementDescriptor;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringContribution;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.core.refactoring.RefactoringDescriptor;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.text.edits.TextEdit;

import com.google.common.base.Objects;

import edu.ustb.sei.mde.smartmerger.java.CodeBase;
import edu.ustb.sei.mde.smartmerger.java.TagInfoPattern;
import edu.ustb.sei.mde.smartmerger.java.TagPatternDictionary;
import edu.ustb.sei.mde.smartmerger.java.edits.CodeEditDescriptor;
import edu.ustb.sei.mde.smartmerger.java.edits.EditKind;

public class ASTMerger extends ASTVisitor {
	
	static public Pattern userDocStartPattern = Pattern.compile("\\s*\\<\\!\\-\\-\\s*begin\\-user\\-doc\\s*\\-\\-\\>\\s*");
	static public Pattern userDocEndPattern = Pattern.compile("\\s*\\<\\!\\-\\-\\s*end\\-user\\-doc\\s*\\-\\-\\>\\s*");

	protected static final class BindingCollector extends ASTVisitor {
		private final Map<Object, RenameInfo> typeNamesToBeRenamed;
		private final Map<Object, RenameInfo> variableNamesToBeRenamed;
		private final Map<Object, RenameInfo> methodNamesToBeRenamed;

		protected BindingCollector(Map<Object, RenameInfo> typeNamesToBeRenamed,
				Map<Object, RenameInfo> variableNamesToBeRenamed, Map<Object, RenameInfo> methodNamesToBeRenamed) {
			this.typeNamesToBeRenamed = typeNamesToBeRenamed;
			this.variableNamesToBeRenamed = variableNamesToBeRenamed;
			this.methodNamesToBeRenamed = methodNamesToBeRenamed;
		}

		protected boolean shouldTrack(Name name) {
			StructuralPropertyDescriptor containingReference = name.getLocationInParent();
			if(containingReference==TypeDeclaration.NAME_PROPERTY ||
					containingReference==MethodDeclaration.NAME_PROPERTY
					|| containingReference==VariableDeclarationFragment.NAME_PROPERTY) {
				return false;
			} else return true;
		}

		@Override
		public boolean visit(QualifiedName node) {
			if(shouldTrack(node)) {
				IBinding binding = node.resolveBinding();
				if(binding!=null) {
					String key = binding.getKey();
					
					RenameInfo info = null;
					if(binding instanceof ITypeBinding) {
						info = typeNamesToBeRenamed.get(key);
					} else if(binding instanceof IMethodBinding) {
						info = methodNamesToBeRenamed.get(key);
					} else if(binding instanceof IVariableBinding) {
						info = variableNamesToBeRenamed.get(key);
					}
					
					if(info!=null) {
						info.referencedPlaces.add(node.getName());
					}
				}
			}
			return false;
		}

		@Override
		public boolean visit(SimpleName node) {
			if(shouldTrack(node)) {
				String name = node.getFullyQualifiedName();
				if(name.equals("this") || name.equals("super")) return true;
				
				IBinding binding = node.resolveBinding();
				if(binding!=null) {
					String key = binding.getKey();
					
					RenameInfo info = null;
					if(binding instanceof ITypeBinding) {
						info = typeNamesToBeRenamed.get(key);
					} else if(binding instanceof IMethodBinding) {
						info = methodNamesToBeRenamed.get(key);
					} else if(binding instanceof IVariableBinding) {
						info = variableNamesToBeRenamed.get(key);
					}
					
					if(info!=null) {
						info.referencedPlaces.add(node);
					}
				}
			}
			return false;
		}
	}

	public ASTMerger(TagPatternDictionary dictionary, CodeBase sourceBase, CodeBase viewBase) {
		super();
		this.dictionary = dictionary;
		this.sourceBase = sourceBase;
		this.viewBase = viewBase;
		this.matcher = new ASTMatcher();
		
		this.editDescriptors = new ArrayList<>();
		this.matchedSourceNodes = new HashSet<>();
		this.unmatchedViewNodes = new HashSet<>();
		
		this.alignmentFromSourceToView = new HashMap<>();
		this.alignmentFromViewToSource = new HashMap<>();
		
		this.deletedTypes = new HashSet<>();
	}
	
	private ASTMatcher matcher;
	private ASTMatcher docMatcher = new ASTMatcher(true);

	private TagPatternDictionary dictionary;
	
	private CodeBase sourceBase;
	private CodeBase viewBase;
	private List<CodeEditDescriptor> editDescriptors;
	
	
	private Set<ASTNode> matchedSourceNodes;
	private Set<ASTNode> unmatchedViewNodes;
	private Set<String> deletedTypes;
	
	private Map<ASTNode, ASTNode> alignmentFromSourceToView;
	private Map<ASTNode, ASTNode> alignmentFromViewToSource;
	
//	private Map<ASTNode, ASTNode> viewToNewViewMap;
//	
//	public ASTNode getNewViewNode(ASTNode oldViewNode) {
//		return viewToNewViewMap.getOrDefault(oldViewNode, oldViewNode);
//	}
	
	private void addAlignment(ASTNode source, ASTNode view) {
		this.alignmentFromSourceToView.put(source, view);
		this.alignmentFromViewToSource.put(view, source);
	}
	
	/**
	 * This is a generic change computer that is intended to take the place of the other concrete change computers.
	 * @param nodeInSource
	 * @param nodeInView
	 * @param clazz
	 * @param tagPat
	 */
	protected void computeHashBasedStructuralChanges(ASTNode nodeInSource, ASTNode nodeInView, Class<?> clazz, TagInfoPattern tagPat) {
		String hash = (String) nodeInSource.getProperty(tagPat.key);
		String latest_hash = (String) nodeInSource.getProperty(tagPat.getLatestedKey());
		String view_hash = (String) nodeInView.getProperty(tagPat.key);
		
		StructuralPropertyDescriptor desc = tagPat.hashDescriptorMap.get(clazz);
		
		Object sourceValue = tagPat.getNodeStructuralProperty(nodeInSource, desc);
		Object viewValue = tagPat.getNodeStructuralProperty(nodeInView, desc);
		
		boolean sourceChanged;
		boolean viewChanged;
		
		if(hash==null || latest_hash==null || view_hash==null) {
			sourceChanged = false;
			
			if(desc instanceof SimplePropertyDescriptor) {
				viewChanged = !Objects.equal(sourceValue, viewValue);
			} else if(desc instanceof ChildPropertyDescriptor) {				
				viewChanged = !matcher.safeSubtreeMatch(sourceValue, viewValue);
			} else {
				viewChanged = !matcher.safeSubtreeListMatch((List<?>)sourceValue, (List<?>)viewValue);
			}
		} else {
			sourceChanged = !Objects.equal(hash, latest_hash);
			viewChanged = !Objects.equal(hash, view_hash);
		}
		computeChangeDescriptor(nodeInSource, nodeInView, sourceChanged, viewChanged, desc, tagPat);
	}

	protected void computeChangeDescriptor(ASTNode nodeInSource, ASTNode nodeInView,
			boolean sourceChanged, boolean viewChanged, StructuralPropertyDescriptor property, TagInfoPattern tagPattern) {
		CodeEditDescriptor descriptor = null;
		
		if(sourceChanged && viewChanged) {
			descriptor = CodeEditDescriptor.createDescriptor(nodeInSource, nodeInView, property, tagPattern.getNodeStructuralProperty(nodeInSource, nodeInSource.getClass()), EditKind.KEEP, tagPattern);
		} else if(sourceChanged) {
			descriptor = CodeEditDescriptor.createDescriptor(nodeInSource, nodeInView, property, tagPattern.getNodeStructuralProperty(nodeInSource, nodeInSource.getClass()), property instanceof ChildListPropertyDescriptor ? EditKind.PUSH_ALL : EditKind.PUSH, tagPattern);
		} else if(viewChanged) {
			descriptor = CodeEditDescriptor.createDescriptor(nodeInSource, nodeInView, property, tagPattern.getNodeStructuralProperty(nodeInView, nodeInView.getClass()), property instanceof ChildListPropertyDescriptor ? EditKind.PULL_ALL : EditKind.PULL, tagPattern);
		} else {
		}
		
		if(descriptor!=null)
			this.editDescriptors.add(descriptor);
	}
	
	@Override
	public boolean visit(FieldDeclaration nodeInView) {
		if(isGeneratedNode(nodeInView)) {
			// modifiers, type, fragment[0], initializer
			String uuid = (String) nodeInView.getProperty(TagPatternDictionary.uuid.key);
			FieldDeclaration nodeInSource = match(uuid, nodeInView);
			if(nodeInSource!=null) {
				matchedSourceNodes.add(nodeInSource);
				addAlignment(nodeInSource, nodeInView);
				
				dictionary.getHashPatterns(FieldDeclaration.class).forEach(tagPat->{
					computeHashBasedStructuralChanges(nodeInSource, nodeInView, FieldDeclaration.class, tagPat);
				});
				
				// check Javadoc if necessary
				checkJavadoc(FieldDeclaration.class, nodeInSource, nodeInView);

			} else {
				// do nothing now, leave this case to parent
				this.unmatchedViewNodes.add(nodeInView);
			}
		} else {
			System.out.println("Unexpected view field!");
		}
		
		return false;
	}
	
	@Override
	public boolean visit(EnumConstantDeclaration nodeInView) {
		if(isGeneratedNode(nodeInView)) {
			// modifiers, type, fragment[0], initializer
			String uuid = (String) nodeInView.getProperty(TagPatternDictionary.uuid.key);
			EnumConstantDeclaration nodeInSource = match(uuid, nodeInView);
			if(nodeInSource!=null) {
				matchedSourceNodes.add(nodeInSource);
				addAlignment(nodeInSource, nodeInView);
				
				dictionary.getHashPatterns(EnumConstantDeclaration.class).forEach(tagPat->{
					computeHashBasedStructuralChanges(nodeInSource, nodeInView, EnumConstantDeclaration.class, tagPat);
				});
				
				// check Javadoc if necessary
				checkJavadoc(EnumConstantDeclaration.class, nodeInSource, nodeInView);

			} else {
				// do nothing now, leave this case to parent
				this.unmatchedViewNodes.add(nodeInView);
			}
		} else {
			System.out.println("Unexpected view field!");
		}
		
		return false;
	}
	
	/**
	 * The default javadoc checker shall generate KEEP edit only with empty tag pattern!!
	 * @param type
	 * @param nodeInSource
	 * @param nodeInView
	 */
	private void checkJavadoc(Class<?> type, BodyDeclaration nodeInSource,
			BodyDeclaration nodeInView) {
		
		if(!docMatcher.safeSubtreeMatch(nodeInSource.getJavadoc(), nodeInView.getJavadoc())) {
			CodeEditDescriptor descriptor = CodeEditDescriptor.createDescriptor(nodeInSource, nodeInView, nodeInSource.getJavadocProperty(), nodeInView.getJavadoc(), EditKind.KEEP, null);
			this.editDescriptors.add(descriptor);
		}
	}

	@Override
	public boolean visit(TypeDeclaration nodeInView) {
		if(isGeneratedNode(nodeInView)) {
			String uuid = (String) nodeInView.getProperty(TagPatternDictionary.uuid.key);
			TypeDeclaration nodeInSource = match(uuid, nodeInView);
			if(nodeInSource!=null) {
				matchedSourceNodes.add(nodeInSource);
				addAlignment(nodeInSource, nodeInView);
				
				dictionary.getHashPatterns(TypeDeclaration.class).forEach(tagPat->{
					computeHashBasedStructuralChanges(nodeInSource, nodeInView, TypeDeclaration.class, tagPat);
				});
				
				checkJavadoc(TypeDeclaration.class, nodeInSource, nodeInView);
				
				return true;
			} else {
				// do nothing now, leave this case to parent
				this.unmatchedViewNodes.add(nodeInView);
			}
		}
		return false;
	}
	
	@Override
	public boolean visit(EnumDeclaration nodeInView) {
		if(isGeneratedNode(nodeInView)) {
			String uuid = (String) nodeInView.getProperty(TagPatternDictionary.uuid.key);
			EnumDeclaration nodeInSource = match(uuid, nodeInView);
			if(nodeInSource!=null) {
				matchedSourceNodes.add(nodeInSource);
				addAlignment(nodeInSource, nodeInView);
				
				dictionary.getHashPatterns(TypeDeclaration.class).forEach(tagPat->{
					computeHashBasedStructuralChanges(nodeInSource, nodeInView, EnumDeclaration.class, tagPat);
				});
				
				checkJavadoc(EnumDeclaration.class, nodeInSource, nodeInView);
				
				return true;
			} else {
				// do nothing now, leave this case to parent
				this.unmatchedViewNodes.add(nodeInView);
			}
		}
		return false;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void endVisit(TypeDeclaration node) {
		if(!this.unmatchedViewNodes.contains(node)) {
			TypeDeclaration sourceNode = (TypeDeclaration) this.alignmentFromViewToSource.get(node);
			// check unmatched members
			List<BodyDeclaration> bodies;
			
			// FIXME: we need to compute the position
			bodies = node.bodyDeclarations();
			for(BodyDeclaration body : bodies) {
				if(this.unmatchedViewNodes.contains(body)) {
					CodeEditDescriptor descriptor = CodeEditDescriptor.createDescriptor(sourceNode, node, TypeDeclaration.BODY_DECLARATIONS_PROPERTY, body, EditKind.PULL, null);
					this.editDescriptors.add(descriptor);
				}
			}
			
			bodies = sourceNode.bodyDeclarations();
			for(BodyDeclaration body : bodies) {
				if(!this.matchedSourceNodes.contains(body)) {
					if(isGeneratedNode(body)) {						
						CodeEditDescriptor descriptor = CodeEditDescriptor.createDescriptor(sourceNode, node, TypeDeclaration.BODY_DECLARATIONS_PROPERTY, body, EditKind.REMOVE, null);
						this.editDescriptors.add(descriptor);
					}
					
				}
			}
		}
	}
	
//	private List<CompilationUnit> matchedViewCompiationUnits = new ArrayList<>();
//	public boolean visit(CompilationUnit unit) {
//		matchedViewCompiationUnits.clear();
//		return false;
//	}
//	
	@SuppressWarnings("unchecked")
	@Override
	public void endVisit(EnumDeclaration node) {
		if(!this.unmatchedViewNodes.contains(node)) {
			EnumDeclaration sourceNode = (EnumDeclaration) this.alignmentFromViewToSource.get(node);
			// check unmatched members
			List<BodyDeclaration> bodies;
			
			bodies = node.bodyDeclarations();
			for(BodyDeclaration body : bodies) {
				if(this.unmatchedViewNodes.contains(body)) {
					CodeEditDescriptor descriptor = CodeEditDescriptor.createDescriptor(sourceNode, node, EnumDeclaration.BODY_DECLARATIONS_PROPERTY, body, EditKind.PULL, null);
					this.editDescriptors.add(descriptor);
				}
			}
			
			bodies = sourceNode.bodyDeclarations();
			for(BodyDeclaration body : bodies) {
				if(!this.matchedSourceNodes.contains(body)) {
					if(isGeneratedNode(body)) {						
						CodeEditDescriptor descriptor = CodeEditDescriptor.createDescriptor(sourceNode, node, EnumDeclaration.BODY_DECLARATIONS_PROPERTY, body, EditKind.REMOVE, null);
						this.editDescriptors.add(descriptor);
					}
					
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void endVisit(CompilationUnit node) {
		TypeDeclaration primaryType = (TypeDeclaration) node.types().get(0);
		if(this.unmatchedViewNodes.contains(primaryType)) {
			CodeEditDescriptor descriptor = CodeEditDescriptor.createDescriptor(null, null, null, node, EditKind.PULL, null);
			this.editDescriptors.add(descriptor);
		} else {
			// we skip view children checking
			AbstractTypeDeclaration sourcePrimaryType = (AbstractTypeDeclaration) this.alignmentFromViewToSource.get(primaryType);
			CompilationUnit sourceNode = (CompilationUnit) sourcePrimaryType.getParent();
			List<AbstractTypeDeclaration> sourceTypes = sourceNode.types();
			for(AbstractTypeDeclaration sourceType : sourceTypes) {
				if(!this.matchedSourceNodes.contains(sourceType)) {
					if(isGeneratedNode(sourceType)) {
						CodeEditDescriptor descriptor = CodeEditDescriptor.createDescriptor(sourceNode, node, CompilationUnit.TYPES_PROPERTY, sourceType, EditKind.REMOVE, null);
						this.editDescriptors.add(descriptor);
					}
				}
			}
			
			// handle imports sourceNode <-> node
			List<ImportDeclaration> sourceImports = sourceNode.imports();
			List<ImportDeclaration> viewImports = node.imports();
//			List<ImportDeclaration> newImports = new ArrayList<>();
			if(matcher.safeSubtreeListMatch(sourceImports, viewImports)==false) {
				CodeEditDescriptor descriptor = CodeEditDescriptor.createPullAllDescriptor(sourceNode, node, CompilationUnit.IMPORTS_PROPERTY, null);
				this.editDescriptors.add(descriptor);
			}
		}
	}

	@Override
	public boolean visit(MethodDeclaration nodeInView) {
		if(isGeneratedNode(nodeInView)) {
			String uuid = (String) nodeInView.getProperty(TagPatternDictionary.uuid.key);
			MethodDeclaration nodeInSource = match(uuid, nodeInView);
			if(nodeInSource!=null) {
				matchedSourceNodes.add(nodeInSource);
				addAlignment(nodeInSource, nodeInView);
				
				dictionary.getHashPatterns(MethodDeclaration.class).forEach(tagPat->{
					// we do not rename constructor!
					if(!nodeInView.isConstructor() || !TagInfoPattern.isEntityName(tagPat.hashDescriptorMap.get(MethodDeclaration.class)))
						computeHashBasedStructuralChanges(nodeInSource, nodeInView, MethodDeclaration.class, tagPat);
				});
				
				checkJavadoc(MethodDeclaration.class, nodeInSource, nodeInView);
			} else {
				// do nothing now, leave this case to parent
				this.unmatchedViewNodes.add(nodeInView);
			}
		}
		return false;
	}

	protected TypeDeclaration match(String uuid, TypeDeclaration nodeInView) {
		TypeDeclaration cor = null;
		if(uuid!=null) {
			cor = (TypeDeclaration) sourceBase.indexBase.getTypeIndex().getASTNodeFromId(uuid);
		}
		
		if(cor==null) {
			// try to match with signature, may have conflict
			IBinding binding = nodeInView.resolveBinding();
			String key = binding.getKey();
			cor = (TypeDeclaration) sourceBase.indexBase.getTypeIndex().getASTNodeFromKey(key);
		}
		
		return cor;
	}
	
	protected EnumDeclaration match(String uuid, EnumDeclaration nodeInView) {
		EnumDeclaration cor = null;
		if(uuid!=null) {
			cor = (EnumDeclaration) sourceBase.indexBase.getTypeIndex().getASTNodeFromId(uuid);
		}
		
		if(cor==null) {
			// try to match with signature, may have conflict
			IBinding binding = nodeInView.resolveBinding();
			String key = binding.getKey();
			cor = (EnumDeclaration) sourceBase.indexBase.getTypeIndex().getASTNodeFromKey(key);
		}
		
		return cor;
	}

	protected MethodDeclaration match(String uuid, MethodDeclaration nodeInView) {
		MethodDeclaration cor = null;
		if(uuid!=null) {
			cor = (MethodDeclaration) sourceBase.indexBase.getMethodIndex().getASTNodeFromId(uuid);
		}
		
		if(cor==null) {
			IBinding binding = nodeInView.resolveBinding();
			String key = binding.getKey();
			TypeDeclaration typeInView = (TypeDeclaration) nodeInView.getParent();
			TypeDeclaration typeInSource = (TypeDeclaration) this.alignmentFromViewToSource.get(typeInView);
			if(typeInSource!=null) {
				cor = (MethodDeclaration) sourceBase.indexBase.getMethodIndex().getASTNodeFromKey(key);
				if(cor!=null && cor.getParent()!=typeInSource) cor = null;
			}
		}
		
		return cor;
	}

	protected FieldDeclaration match(String uuid, FieldDeclaration nodeInView) {
		FieldDeclaration cor = null;
		if(uuid!=null) {
			cor = (FieldDeclaration) sourceBase.indexBase.getFieldIndex().getASTNodeFromId(uuid);
		}
		
		if(cor==null) {
			VariableDeclarationFragment fragment = (VariableDeclarationFragment) nodeInView.fragments().get(0);
			IBinding binding = fragment.resolveBinding();
			String key = binding.getKey();
			TypeDeclaration typeInView = (TypeDeclaration) nodeInView.getParent();
			TypeDeclaration typeInSource = (TypeDeclaration) this.alignmentFromViewToSource.get(typeInView);
			if(typeInSource!=null) {
				cor = (FieldDeclaration) sourceBase.indexBase.getFieldIndex().getASTNodeFromKey(key);
				if(cor!=null && cor.getParent()!=typeInSource) cor = null;
			}
		}
		
		return cor;
	}
	
	protected EnumConstantDeclaration match(String uuid, EnumConstantDeclaration nodeInView) {
		EnumConstantDeclaration cor = null;
		if(uuid!=null) {
			cor = (EnumConstantDeclaration) sourceBase.indexBase.getFieldIndex().getASTNodeFromId(uuid);
		}
		
		if(cor==null) {
			EnumDeclaration typeInView = (EnumDeclaration) nodeInView.getParent();
			EnumDeclaration typeInSource = (EnumDeclaration) this.alignmentFromViewToSource.get(typeInView);
			if(typeInSource!=null) {
				cor = (EnumConstantDeclaration) sourceBase.indexBase.getFieldIndex().getASTNodeFromKey(nodeInView.resolveVariable().getKey());
				if(cor!=null && cor.getParent()!=typeInSource) cor = null;
			}
		}
		
		return cor;
	}
	
	@SuppressWarnings("unchecked")
	protected void checkUnmatchedSourceCompilationUnits() {
		this.sourceBase.compilationUnitMap.values().forEach(cu->{
			if(cu.types().stream().allMatch(t->{
				if(isGeneratedNode(t) 
						&& !this.matchedSourceNodes.contains((ASTNode)t)) {
					this.deletedTypes.add(this.sourceBase.indexBase.getTypeIndex().getIDFromASTNode((ASTNode) t));
					return true;
				} else return false;
			})) {
				CodeEditDescriptor descriptor = CodeEditDescriptor.createDescriptor(null, null, null, cu, EditKind.REMOVE, null);
				this.editDescriptors.add(descriptor);
			}
		});
	}

	public boolean isGeneratedNode(Object node) {
		return ((ASTNode)node).getProperty(TagPatternDictionary.isGenerated.key)!=null;
	}
	
	public void doMatch() {
		ASTHasher sourceHasher = new ASTHasher(this.dictionary, new CodeNormalizer(sourceBase.indexBase));
		ASTHasher viewHasher = new ASTHasher(this.dictionary, new CodeNormalizer(viewBase.indexBase), true);
		
		this.sourceBase.compilationUnitMap.values().forEach(cu->{
			cu.accept(sourceHasher); // compute latest hash
		});
		
		this.viewBase.compilationUnitMap.values().forEach(cu->{
			cu.accept(viewHasher);
			cu.accept(this);
		});
		
		checkUnmatchedSourceCompilationUnits();
		
		applyChanges();
	}
	
	public void applyChanges() {
		List<CodeEditDescriptor> pushEdits = new ArrayList<>(this.editDescriptors.size());
		List<CodeEditDescriptor> pullNameEdits = new ArrayList<>(this.editDescriptors.size());
		List<CodeEditDescriptor> pullNonNameEdits = new ArrayList<>(this.editDescriptors.size());
		List<CodeEditDescriptor> removeEdits = new ArrayList<>(this.editDescriptors.size()/2);
		List<CodeEditDescriptor> keepEdits = new ArrayList<>(this.editDescriptors.size()/2);
		
		this.editDescriptors.forEach(it->{
			switch(it.kind) {
			case PUSH:
			case PUSH_ALL:
				pushEdits.add(it);
				break;
			case PULL:
			case PULL_ALL:
				if(TagInfoPattern.isEntityName(it.nodeProperty)) 
					pullNameEdits.add(it);
				else pullNonNameEdits.add(it);
				break;
			case REMOVE:
				removeEdits.add(it);
				break;
			case KEEP:
				if(TagInfoPattern.isEntityName(it.nodeProperty))
					pushEdits.add(it);
				else
					keepEdits.add(it);
				break;
			}
		});
		
		if(pullNameEdits.isEmpty() && pullNonNameEdits.isEmpty() &&removeEdits.isEmpty() && keepEdits.isEmpty()) return;
		
		pushChanges(pushEdits);
		
		// remove
		// pull-insert/replace
		// keep
		pullNonRefactoringChanges(removeEdits, pullNonNameEdits, keepEdits);
		
		// pull-refactor
		pullRefactorChanges(pullNameEdits);
		
	}

	private void pullRefactorChanges(List<CodeEditDescriptor> pullNameEdits) {
		Set<Supplier<Boolean>> typeRenameEdits = new HashSet<>();
		Set<Supplier<Boolean>> variableRenameEdits = new HashSet<>();
		Set<Supplier<Boolean>> methodRenameEdits = new HashSet<>();
		
		pullNameEdits.stream().forEach(edit->{
			try {
				if(edit.nodeProperty!=null 
						&& edit.tagPattern!=null 
						&& TagInfoPattern.isEntityName(edit.nodeProperty)) {
					Name sourceName = (Name) edit.tagPattern.getNodeStructuralProperty(edit.nodeInSource, edit.nodeProperty);
					IBinding binding = sourceName.resolveBinding();
					switch(binding.getKind()) {
					case IBinding.TYPE:
					{
						RefactoringContribution contribution = RefactoringCore.getRefactoringContribution(IJavaRefactorings.RENAME_TYPE);
						RenameJavaElementDescriptor descriptor = (RenameJavaElementDescriptor) contribution.createDescriptor();
						descriptor.setProject(binding.getJavaElement().getJavaProject().getElementName());
						descriptor.setNewName(((Name)edit.propertyValue).getFullyQualifiedName()); // new name for a Class
						descriptor.setJavaElement(binding.getJavaElement());
						typeRenameEdits.add(()->{
							descriptor.setUpdateReferences(true);
							try {
								return applyRefactoring(descriptor);
							} catch (Exception e) {
								e.printStackTrace();
								return false;
							}
						});
						break;
					}
					case IBinding.VARIABLE:
					{
						RefactoringContribution contribution = RefactoringCore.getRefactoringContribution(IJavaRefactorings.RENAME_FIELD);
						RenameJavaElementDescriptor descriptor = (RenameJavaElementDescriptor) contribution.createDescriptor();
						descriptor.setProject(binding.getJavaElement().getJavaProject().getElementName());
						descriptor.setNewName(((Name)edit.propertyValue).getFullyQualifiedName()); // new name for a Class
						descriptor.setJavaElement(binding.getJavaElement());
						descriptor.setUpdateReferences(true);
						variableRenameEdits.add(()->{
							try {
								return applyRefactoring(descriptor);
							} catch (Exception e) {
								e.printStackTrace();
								return false;
							}
						});
						break;
					}
					case IBinding.METHOD:
					{
						RefactoringContribution contribution = null;
						contribution = RefactoringCore.getRefactoringContribution(IJavaRefactorings.RENAME_METHOD);
						RenameJavaElementDescriptor descriptor = (RenameJavaElementDescriptor) contribution.createDescriptor();
						descriptor.setProject(binding.getJavaElement().getJavaProject().getElementName());
						descriptor.setNewName(((Name)edit.propertyValue).getFullyQualifiedName()); // new name for a Class
						descriptor.setJavaElement(binding.getJavaElement());
						descriptor.setUpdateReferences(true);
						methodRenameEdits.add(()->{
							try {
								return applyRefactoring(descriptor);
							} catch (Exception e) {
								e.printStackTrace();
								return false;
							}
						});
						break;
					}
					}
				} else 
					pushReferencesOrLiterals(edit);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		
		// call refactor APIs to rename field/method/type
		// rebuilt view code base
		
		variableRenameEdits.forEach(edit->{
			edit.get();
		});
		methodRenameEdits.forEach(edit->{
			edit.get();
		});
		typeRenameEdits.forEach(edit->{
			edit.get();
		});
	}
	
	private boolean applyRefactoring(RefactoringDescriptor descriptor) {
		RefactoringStatus status = new RefactoringStatus();
		try {
		    Refactoring refactoring = descriptor.createRefactoring(status);

		    IProgressMonitor monitor = new NullProgressMonitor();
		    refactoring.checkInitialConditions(monitor);
		    refactoring.checkFinalConditions(monitor);
		    Change change = refactoring.createChange(monitor);
		    change.perform(monitor);
		    return true;
		} catch (Exception e) {
		   return false;
		}
	}

	@SuppressWarnings("unchecked")
	private void pullNonRefactoringChanges(List<CodeEditDescriptor> removeEdits,
			List<CodeEditDescriptor> pullNonNameEdits, List<CodeEditDescriptor> keepEdits) {
		Map<CompilationUnit, ASTRewrite> rewriterMap = new HashMap<>();
		
		// collect entities to be removed
		List<CompilationUnit> compilationUnitsToBeRemoved = new ArrayList<>(removeEdits.size()/2);
		List<TypeDeclaration> typesToBeRemoved = new ArrayList<>(removeEdits.size()/2);
		List<MethodDeclaration> methodsToBeRemoved = new ArrayList<>(removeEdits.size()/2);
		List<FieldDeclaration> fieldsToBeRemoved = new ArrayList<>(removeEdits.size()/2);
		
		for(CodeEditDescriptor desc : removeEdits) {
			if(desc.propertyValue instanceof CompilationUnit) compilationUnitsToBeRemoved.add((CompilationUnit) desc.propertyValue);
			else if(desc.propertyValue instanceof TypeDeclaration) typesToBeRemoved.add((TypeDeclaration) desc.propertyValue);
			else if(desc.propertyValue instanceof TypeDeclaration) typesToBeRemoved.add((TypeDeclaration) desc.propertyValue);
			else if(desc.propertyValue instanceof MethodDeclaration) methodsToBeRemoved.add((MethodDeclaration) desc.propertyValue);
			else if(desc.propertyValue instanceof FieldDeclaration) fieldsToBeRemoved.add((FieldDeclaration) desc.propertyValue);
			else {
				System.err.println("Error: Unknown entity "+desc.propertyValue);
			}
		}
		
		
		for(FieldDeclaration node : fieldsToBeRemoved) {
			CompilationUnit cu  = (CompilationUnit) node.getRoot();
			ASTRewrite rewriter = rewriterMap.computeIfAbsent(cu, c->ASTRewrite.create(c.getAST()));
			ListRewrite lr = rewriter.getListRewrite(node.getParent(), (ChildListPropertyDescriptor) node.getLocationInParent());
			lr.remove(node, null);
		}
		
		for(MethodDeclaration node : methodsToBeRemoved) {
			CompilationUnit cu  = (CompilationUnit) node.getRoot();
			ASTRewrite rewriter = rewriterMap.computeIfAbsent(cu, c->ASTRewrite.create(c.getAST()));
			ListRewrite lr = rewriter.getListRewrite(node.getParent(), (ChildListPropertyDescriptor) node.getLocationInParent());
			lr.remove(node, null);
		}
		
		for(TypeDeclaration node : typesToBeRemoved) {
			CompilationUnit cu  = (CompilationUnit) node.getRoot();
			ASTRewrite rewriter = rewriterMap.computeIfAbsent(cu, c->ASTRewrite.create(c.getAST()));
			ListRewrite lr = rewriter.getListRewrite(node.getParent(), (ChildListPropertyDescriptor) node.getLocationInParent());
			lr.remove(node, null);
		}
		
		// pull non-name changes
		for(CodeEditDescriptor edit : pullNonNameEdits) {
			// first, we need to know if it is a insertion of compilation unit
			
			if(edit.nodeProperty==null && edit.propertyValue instanceof CompilationUnit) {
				addCompilationUnit(this.sourceBase.project, (CompilationUnit) edit.propertyValue);
			} else {
				ASTNode sourceNodeToBeChanged = null;
				if(edit.nodeProperty.getNodeClass()==VariableDeclarationFragment.class) {
					sourceNodeToBeChanged = (ASTNode) ((FieldDeclaration)edit.nodeInSource).fragments().get(0);
				} else sourceNodeToBeChanged = edit.nodeInSource;

				CompilationUnit cu = (CompilationUnit) edit.nodeInSource.getRoot();
				ASTRewrite rewriter = rewriterMap.computeIfAbsent(cu, c->ASTRewrite.create(c.getAST()));
				
				// secondly, we must check if it is a insertion or a replace when the first case is broken
				if(edit.nodeProperty instanceof ChildListPropertyDescriptor && edit.propertyValue instanceof ASTNode) {
					// insertion
					ListRewrite lr = rewriter.getListRewrite(sourceNodeToBeChanged, (ChildListPropertyDescriptor) edit.nodeProperty);
					// FIXME: insert a static field at the end may cause some problems => order of declaration matters here!
					lr.insertLast(ASTNode.copySubtree(cu.getAST(), (ASTNode) edit.propertyValue), null);
				} else {
					if(edit.nodeProperty==CompilationUnit.IMPORTS_PROPERTY) {
						mergeImports(edit, rewriter);
					} else {
						// replace
						if(edit.nodeProperty instanceof ChildListPropertyDescriptor) {
							ListRewrite lr = rewriter.getListRewrite(sourceNodeToBeChanged, (ChildListPropertyDescriptor) edit.nodeProperty);
							mergeNodeList(lr, sourceNodeToBeChanged, edit.nodeProperty, (List<ASTNode>) edit.propertyValue);
						} else {
							if(edit.nodeProperty instanceof SimplePropertyDescriptor) {
								rewriter.set(sourceNodeToBeChanged, edit.nodeProperty, edit.propertyValue, null);
							} else {
								rewriter.set(sourceNodeToBeChanged, edit.nodeProperty, edit.propertyValue==null ? null : ASTNode.copySubtree(cu.getAST(), (ASTNode)edit.propertyValue), null);
							}
						}
					}
					
				}
			}
		}
		
		for(CodeEditDescriptor edit : keepEdits) {
			// in general, we keep source unchanged
			// the exceptions are modifiers and javadoc
			
			if(isModifierProperty(edit.nodeProperty)) {
				CompilationUnit cu = (CompilationUnit) edit.nodeInSource.getRoot();
				ASTRewrite rewriter = rewriterMap.computeIfAbsent(cu, c->ASTRewrite.create(c.getAST()));
				ListRewrite lr = rewriter.getListRewrite(edit.nodeInSource, (ChildListPropertyDescriptor) edit.nodeProperty);
				keepAndMergeModifierList(lr, edit.nodeInSource, (List<ASTNode>) edit.propertyValue);
			} else if(TagInfoPattern.isJavadoctProperty(edit.nodeProperty)) {
				CompilationUnit cu = (CompilationUnit) edit.nodeInSource.getRoot();
				ASTRewrite rewriter = rewriterMap.computeIfAbsent(cu, c->ASTRewrite.create(c.getAST()));
				keepAndMergeJavadoc(rewriter, edit.nodeInSource, (Javadoc) edit.propertyValue);
			}
		}
		
		// apply AST changes
		
		rewriterMap.entrySet().forEach(e->{
			try {
				ICompilationUnit icu = (ICompilationUnit) (e.getKey().getJavaElement());
				TextEdit edit = e.getValue().rewriteAST();

				NullProgressMonitor monitor = new NullProgressMonitor();
				ICompilationUnit workingCopy = icu.getWorkingCopy(monitor);
				workingCopy.applyTextEdit(edit, monitor);
				workingCopy.reconcile(ICompilationUnit.NO_AST, false, null, monitor);
				workingCopy.commitWorkingCopy(true, monitor);
				
			} catch (JavaModelException | IllegalArgumentException e1) {
				e1.printStackTrace();
			}
		});
		
		// delete compilation units
		for(CompilationUnit cu : compilationUnitsToBeRemoved) {
			ICompilationUnit icu = (ICompilationUnit) cu.getJavaElement();
			
			try {
				icu.delete(true, new NullProgressMonitor());
			} catch (JavaModelException e) {
				e.printStackTrace();
			}
		}
	}

	protected void mergeImports(CodeEditDescriptor edit, ASTRewrite rewriter) {
		List<ImportDeclaration> sourceImports = ((CompilationUnit)edit.nodeInSource).imports();
		List<ImportDeclaration> viewImports = ((CompilationUnit)edit.nodeInView).imports();
		ListRewrite lr = rewriter.getListRewrite(edit.nodeInSource, (ChildListPropertyDescriptor) edit.nodeProperty);
		
		viewImports.forEach(i->{
			if(sourceImports.stream().noneMatch(si->{
				return matcher.safeSubtreeMatch(si.getName(), i.getName());
			})) {
				lr.insertLast(ASTNode.copySubtree(edit.nodeInSource.getAST(), i), null);
			}
		});
		sourceImports.forEach(i->{
			String importedName = i.getName().getFullyQualifiedName();
			if(isDeletedType(importedName)) {
				lr.remove(i, null);
			}
		});
	}
	
	private boolean isDeletedType(String importedName) {
		return deletedTypes.contains(importedName);
	}

	@SuppressWarnings("unchecked")
	private void keepAndMergeJavadoc(ASTRewrite rewriter, ASTNode nodeInSource, Javadoc viewdoc) {
		if(viewdoc!=null) {
			ChildPropertyDescriptor javadocProperty = ((BodyDeclaration)nodeInSource).getJavadocProperty();
			
			Javadoc sourcedoc = (Javadoc) nodeInSource.getStructuralProperty(javadocProperty);
			if(sourcedoc==null) {
				rewriter.set(nodeInSource, javadocProperty, ASTNode.copySubtree(nodeInSource.getAST(), viewdoc), null); // replace directly
			} else {
				TagElement sourcePlainDoc = getPlainDoc(sourcedoc);
				TagElement viewPlainDoc = getPlainDoc(viewdoc);
				
				ListRewrite lr = rewriter.getListRewrite(sourcedoc, Javadoc.TAGS_PROPERTY);
				
				// preserve user docs
				if(sourcePlainDoc!=null && viewPlainDoc!=null) {
					List<IDocElement> sourceFragments = sourcePlainDoc.fragments();
					List<TextElement> userDocs = new ArrayList<>();
					boolean inUserDoc = false;
					for(IDocElement elem : sourceFragments) {
						if(elem instanceof TextElement) {
							if(inUserDoc) {
								if (userDocEndPattern.asMatchPredicate().test(((TextElement)elem).getText())) {
									inUserDoc = false;
								} else {
									((TextElement)elem).setText(((TextElement)elem).getText());
									userDocs.add((TextElement) elem);
								}
							} else {
								if (userDocStartPattern.asMatchPredicate().test(((TextElement)elem).getText())) {
									inUserDoc = true;
								}
							}
						}
					}
					
					boolean filled = false;
					inUserDoc = false;
					
					List<IDocElement> viewFragments = viewPlainDoc.fragments();
					ListIterator<IDocElement> iter = viewFragments.listIterator();
					int startIndex = -1;
					
					while(iter.hasNext()) {
						IDocElement elem = iter.next();
						if(elem instanceof TextElement) {
							if(filled) {
								if(inUserDoc) {
									iter.remove();
									if (userDocEndPattern.asMatchPredicate().test(((TextElement)elem).getText())) {
										inUserDoc = false;
									}
								} else {
									if (userDocStartPattern.asMatchPredicate().test(((TextElement)elem).getText())) {
										inUserDoc = true;
										iter.remove();
									}
								}
							} else {
								if(inUserDoc) {
									if (userDocEndPattern.asMatchPredicate().test(((TextElement)elem).getText())) {
										inUserDoc = false;
										filled = true;
									} else {
										iter.remove();
									}
								} else {
									if (userDocStartPattern.asMatchPredicate().test(((TextElement)elem).getText())) {
										((TextElement)elem).setText(((TextElement)elem).getText());
										startIndex = iter.nextIndex();
										inUserDoc = true;
									}
								}
							}
						}
					}
					
					if(startIndex==-1) {
						TextElement end = viewPlainDoc.getAST().newTextElement();
						end.setText("<!-- end-user-doc -->");
						viewPlainDoc.fragments().add(0, end);
						
						TextElement start = viewPlainDoc.getAST().newTextElement();
						start.setText("<!-- begin-user-doc -->\n");
						viewPlainDoc.fragments().add(0, start);
						
						startIndex = 1;
					}
					
					viewPlainDoc.fragments().addAll(startIndex, ASTNode.copySubtrees(viewPlainDoc.getAST(), userDocs));
					lr.remove(sourcePlainDoc, null);
					lr.insertFirst(wrapLines(ASTNode.copySubtree(rewriter.getAST(), viewPlainDoc)), null);
				} else if(sourcePlainDoc==null && viewPlainDoc!=null) {
					lr.insertFirst(wrapLines(ASTNode.copySubtree(rewriter.getAST(), viewPlainDoc)), null);
				}
				
				// update hash if sourcehash_latest==viewhash
				// compute overwriteable tags
				Set<String> replaceableTags = new HashSet<>();
				Stream<TagInfoPattern> tagPatterns = dictionary.getHashPatterns(nodeInSource.getClass());
				tagPatterns.forEach(tag->{
					String hash = (String) nodeInSource.getProperty(tag.key);
					String hash_last = (String) nodeInSource.getProperty(tag.getLatestedKey());
					if(Objects.equal(hash, hash_last)) {
						replaceableTags.add(tag.key);
					}
				});
				
				// overwrite
				sourcedoc.tags().forEach(tag->{
					String name = ((TagElement)tag).getTagName();
					if(name!=null && replaceableTags.contains(name)) {
						String hash = (String) nodeInSource.getProperty(name);
						// FIXME: here, name is a tag name, rather than a hash key!
						String viewValue = (String) viewdoc.getParent().getProperty(name);
						if(viewValue!=null) {
							if(!viewValue.equals(hash)) {
								TagElement newTag = rewriter.getAST().newTagElement();
								newTag.setTagName(name);
								TextElement text = rewriter.getAST().newTextElement();
								text.setText(viewValue);
								newTag.fragments().add(text);
								lr.replace((ASTNode) tag, newTag, null);
							}
						}
					}
				});
				
				// replace other tag
				sourcedoc.tags().forEach(tag->{
					if(!shouldIgnore((TagElement) tag))
						lr.remove((ASTNode) tag, null);
				});
				
				viewdoc.tags().forEach(tag->{
					TagElement te = (TagElement)tag;
					if(te.getTagName()!=null && 
							(!shouldIgnore(te) || !hasTagElement(sourcedoc, te))) {
						TagElement se = (TagElement) sourcedoc.tags().stream()
								.filter(it->Objects.equal(((TagElement)it).getTagName(), te.getTagName()))
								.findAny().orElse(null);
						if(se==null)
							lr.insertLast(ASTNode.copySubtree(rewriter.getAST(), te), null);
						else lr.replace(se, ASTNode.copySubtree(rewriter.getAST(), te), null);
					}
				});
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private ASTNode wrapLines(ASTNode node) {
		if(node instanceof TagElement) {
			List<IDocElement> children = ((TagElement) node).fragments();
			
			if(children.size()>0 && !(children.get(0) instanceof TextElement)) {
				TextElement firstText = node.getAST().newTextElement();
				firstText.setText(" * ");
				children.add(0, firstText);
			}
			
			// line breaks should be added between two successive text elements
			for(int i=0;i<children.size();i++) {
				IDocElement cur = children.get(i);
				if(cur instanceof TextElement) {
					if(i<children.size()-1) {
						IDocElement next = children.get(i+1);
						if(next instanceof TextElement) {
							((TextElement) cur).setText(((TextElement)cur).getText()+System.lineSeparator());
							((TextElement) next).setText(" * "+((TextElement) next).getText());
						}
					}
				}
			}
		}
		return node;
	}
	
	@SuppressWarnings("unchecked")
	private boolean hasTagElement(Javadoc doc, TagElement element) {
		List<TagElement> tags = doc.tags();
		return tags.stream().anyMatch(e->this.docMatcher.safeSubtreeMatch(e, element));
	}
	
	private boolean shouldIgnore(TagElement element) {
		String tagName = element.getTagName();
		return tagName == null ||
				tagName.equals("@generated") || tagName.equals("@model") || 
				tagName.equals("@uuid") || (dictionary.isTag(tagName));
	}

	static private TagElement getPlainDoc(Javadoc sourcedoc) {
		@SuppressWarnings("unchecked")
		List<TagElement> tags = sourcedoc.tags();
		return tags.stream().filter(t->t.getTagName()==null || t.getTagName().isBlank()).findAny().orElse(null);
	}

	@SuppressWarnings({ "unchecked", "unlikely-arg-type" })
	private void keepAndMergeModifierList(ListRewrite lr, ASTNode nodeInSource,
			List<ASTNode> viewValue) {
		List<ASTNode> copiedList = ASTNode.copySubtrees(lr.getASTRewrite().getAST(), viewValue);
		List<ASTNode> sourceList = lr.getOriginalList();
		
		for(int vi=0; vi<copiedList.size();vi++) {
			IExtendedModifier ve =(IExtendedModifier) copiedList.get(vi);
			if(ve.isModifier()) {				
				Modifier vme = (Modifier) ve;
				if(vme.isPrivate() || vme.isPublic() || vme.isProtected()) {
					if(sourceList.stream().filter(it->((IExtendedModifier)it).isModifier()).map(it->(Modifier)it).noneMatch(it->it.isPublic() || it.isPrivate() || it.isProtected())) {
						lr.insertLast(vme, null);
					}
				} else if(vme.isAbstract() && nodeInSource instanceof MethodDeclaration) {
					if(((MethodDeclaration)nodeInSource).getBody()==null) {
						lr.insertLast(vme, null);
					}
				} else {
					lr.insertLast(vme, null);
				}
			} else {
				if(((Annotation)ve).getTypeName().equals(Override.class.getName())) continue;
				
				if(sourceList.stream().noneMatch(e->e.subtreeMatch(this.matcher, ve)))
					lr.insertLast((ASTNode) ve, null);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void mergeNodeList(ListRewrite lr, ASTNode nodeInSource, StructuralPropertyDescriptor nodeProperty,
			List<ASTNode> viewValue) {
		// we must consider different cases for merging
		
		// case 1. modifiers
		if(isModifierProperty(nodeProperty)) {
			// copy new modifier to source
			List<ASTNode> copiedList = ASTNode.copySubtrees(lr.getASTRewrite().getAST(), viewValue);
			List<ASTNode> sourceList = lr.getOriginalList();
			
			int si, vi;
			for(si=0, vi=0; si<sourceList.size() && vi<copiedList.size();) {
				IExtendedModifier se =(IExtendedModifier) sourceList.get(si);
				IExtendedModifier ve =(IExtendedModifier) copiedList.get(vi);
				
				if(se.isModifier() && ve.isModifier()) {					
					lr.replace((ASTNode)se, (ASTNode)ve, null);
					si ++;
					vi ++;
				} else if(se.isAnnotation() && ve.isModifier()) {
					vi ++;
					lr.insertAfter((ASTNode)se, (ASTNode)ve, null);
				} else if(se.isModifier() && ve.isAnnotation()) {
					vi ++;
					if(sourceList.stream().noneMatch(e->e.subtreeMatch(this.matcher, ve)))
						lr.insertBefore((ASTNode)se, (ASTNode)ve, null);
				} else {
					si ++;
					vi ++;
					if(sourceList.stream().noneMatch(e->e.subtreeMatch(this.matcher, ve)))
						lr.insertAfter((ASTNode)se, (ASTNode)ve, null);
				}
			}
			
			for(;si<sourceList.size();si++) {
				IExtendedModifier se =(IExtendedModifier) sourceList.get(si);
				if(se.isModifier()) lr.remove((ASTNode) se, null);
			}
			
			for(;vi<copiedList.size();vi++) {
				IExtendedModifier ve =(IExtendedModifier) copiedList.get(vi);
				if(ve.isModifier() || sourceList.stream().noneMatch(e->e.subtreeMatch(this.matcher, ve)))
					lr.insertLast((ASTNode) ve, null);
			}
		} else {
			List<ASTNode> copiedList = ASTNode.copySubtrees(lr.getASTRewrite().getAST(), viewValue);
			List<ASTNode> sourceList = lr.getOriginalList();
			sourceList.forEach(e->lr.remove(e, null));
			copiedList.forEach(e->lr.insertLast(e, null));
		}
	}

	static public boolean isModifierProperty(StructuralPropertyDescriptor nodeProperty) {
		return nodeProperty==FieldDeclaration.MODIFIERS2_PROPERTY 
				|| nodeProperty==MethodDeclaration.MODIFIERS2_PROPERTY 
				|| nodeProperty==TypeDeclaration.MODIFIERS2_PROPERTY;
	}

	private void addCompilationUnit(IJavaProject project, CompilationUnit compilationUnitToBePulled) {
		try {
			String packageName = compilationUnitToBePulled.getPackage().getName().getFullyQualifiedName();
			ICompilationUnit viewCompilationUnit = (ICompilationUnit) compilationUnitToBePulled.getJavaElement();
			IPackageFragment viewPackageFragment =  (IPackageFragment) viewCompilationUnit.getParent();
			IPackageFragmentRoot viewPackageFragmentRoot = (IPackageFragmentRoot) viewPackageFragment.getParent();
			
			IPackageFragmentRoot sourcePackageFragmentRoot = null;
			IPackageFragmentRoot firstSourcePackageFragmentRoot = null;
			
			for(IPackageFragmentRoot root : project.getAllPackageFragmentRoots()) {
				if(!root.isArchive()) {
					if(firstSourcePackageFragmentRoot==null) firstSourcePackageFragmentRoot = root;
					if(root.getElementName().equals(viewPackageFragmentRoot.getElementName())) {
						sourcePackageFragmentRoot = root;
						break;
					}
				}
			}
			
			if(sourcePackageFragmentRoot==null) {
				sourcePackageFragmentRoot = firstSourcePackageFragmentRoot;
			}
			
			IPackageFragment sourcePackageFragment = sourcePackageFragmentRoot.getPackageFragment(packageName);
			if(sourcePackageFragment.exists()==false) {
				sourcePackageFragmentRoot.createPackageFragment(packageName, true, null);
			}
			
			sourcePackageFragment.createCompilationUnit(viewCompilationUnit.getElementName(), viewCompilationUnit.getSource(), false, null);
			
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
	}

	protected void pushChanges(List<CodeEditDescriptor> pushEdits) {
		if(!pushEdits.isEmpty()) {
			Map<Object, RenameInfo> typeNamesToBeRenamed = new HashMap<>();
			Map<Object, RenameInfo> variableNamesToBeRenamed = new HashMap<>();
			Map<Object, RenameInfo> methodNamesToBeRenamed = new HashMap<>();
			pushEdits.forEach(edit->{
				try {
					if(edit.nodeProperty!=null 
							&& edit.tagPattern!=null 
							&& TagInfoPattern.isEntityName(edit.nodeProperty)) {
						Name viewName = (Name) edit.tagPattern.getNodeStructuralProperty(edit.nodeInView, edit.nodeProperty);
						IBinding binding = viewName.resolveBinding();
						switch(binding.getKind()) {
						case IBinding.TYPE: {
							RenameInfo info = new RenameInfo(edit.nodeInView, ((Name)edit.propertyValue).getFullyQualifiedName());
							if(info.binding!=null) {
								typeNamesToBeRenamed.put(info.binding.getKey(), info);
							} else {
								typeNamesToBeRenamed.put(info, info); // in this way, we cannot retrieve the value by the key (there is no key)
							}
						}	
						break;
						case IBinding.VARIABLE:{
							RenameInfo info = new RenameInfo(edit.nodeInView, ((Name)edit.propertyValue).getFullyQualifiedName());
							if(info.binding!=null) {
								variableNamesToBeRenamed.put(info.binding.getKey(), info);
							} else {
								variableNamesToBeRenamed.put(info, info); // in this way, we cannot retrieve the value by the key (there is no key)
							}
						}
						break;
						case IBinding.METHOD: {
							RenameInfo info = new RenameInfo(edit.nodeInView, ((Name)edit.propertyValue).getFullyQualifiedName());
							if(info.binding!=null) {
								methodNamesToBeRenamed.put(info.binding.getKey(), info);
							} else {
								methodNamesToBeRenamed.put(info, info); // in this way, we cannot retrieve the value by the key (there is no key)
							}
						}
						break;
						}
					} else 
						pushReferencesOrLiterals(edit);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			
			
			ASTVisitor bindingCollector = new BindingCollector(typeNamesToBeRenamed, variableNamesToBeRenamed, methodNamesToBeRenamed);
			this.viewBase.compilationUnitMap.values().forEach(cu->{
				cu.accept(bindingCollector);
			});
			
			typeNamesToBeRenamed.values().forEach(info->{
				doRenameAtAST(info);
			});
			
			variableNamesToBeRenamed.values().forEach(info->{
				doRenameAtAST(info);
			});
			
			methodNamesToBeRenamed.values().forEach(info->{
				doRenameAtAST(info);
			});
		}
	}

	public void doRenameAtAST(RenameInfo info) {
		info.referencedPlaces.forEach(name->{
			name.setIdentifier(info.newName);
		});
		
		if(info.actualNode instanceof TypeDeclaration) {
			((TypeDeclaration)info.actualNode).getName().setIdentifier(info.newName);
		} else if(info.actualNode instanceof MethodDeclaration) {
			((MethodDeclaration)info.actualNode).getName().setIdentifier(info.newName);
		} else if(info.actualNode instanceof FieldDeclaration) {
			((VariableDeclarationFragment)(((FieldDeclaration)info.actualNode).fragments().get(0))).getName().setIdentifier(info.newName);
		}
	}
	
	private void pushReferencesOrLiterals(CodeEditDescriptor edit) {
		// It seems that there is no need to push references and literals because they won't
		// affect other parts of the generated code and will be preserved during pull
	}
}
