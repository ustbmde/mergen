package edu.ustb.sei.mde.smartmerger.java;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.IBinding;

public class CodeIndex {
	private Map<IBinding, CodeIndexItem> binding2ItemMap = new HashMap<>();
	private Map<ASTNode, CodeIndexItem> astNode2ItemMap = new HashMap<>();
	private Map<String, CodeIndexItem> id2ItemMap = new HashMap<>();
	private Map<String, CodeIndexItem> key2ItemMap = new HashMap<>();
	
	public void addIndex(IBinding binding, ASTNode node, String uuid) {
		String key = binding.getKey();
		CodeIndexItem item = new CodeIndexItem(binding, node, key, uuid);
		
		astNode2ItemMap.put(node, item);
		
		if(binding!=null)
			binding2ItemMap.put(binding, item);
		
		if(uuid!=null)
			id2ItemMap.put(uuid, item);
		
		if(key!=null)
			key2ItemMap.put(key, item);
	}
	
	public String getKeyFromBinding(IBinding binding) {
		return binding.getKey();
	}
	
	public String getKeyFromASTNode(ASTNode node) {
		CodeIndexItem item = astNode2ItemMap.get(node);
		if(item==null) return null;
		else return item.key;
	}
	
	public String getIDFromBinding(IBinding binding) {
		CodeIndexItem item = binding2ItemMap.get(binding);
		if(item==null) return null;
		else return item.uuid;
	}
	
	public String getIDFromASTNode(ASTNode node) {
		CodeIndexItem item = astNode2ItemMap.get(node);
		if(item==null) return null;
		else return item.uuid;
	}
	
	public String getIDFromKey(String key) {
		CodeIndexItem item = key2ItemMap.get(key);
		if(item==null) return null;
		else return item.uuid;
	}

	public String getKeyFromId(String id) {
		CodeIndexItem item = id2ItemMap.get(id);
		if(item==null) return null;
		else return item.key;
	}
	
	public ASTNode getASTNodeFromId(String id) {
		CodeIndexItem item = id2ItemMap.get(id);
		if(item==null) return null;
		else return item.astNode;
	}
	
	public ASTNode getASTNodeFromKey(String key) {
		CodeIndexItem item = key2ItemMap.get(key);
		if(item==null) return null;
		else return item.astNode;
	}
	
	class CodeIndexItem {
		public CodeIndexItem(IBinding binding, ASTNode astNode, String key, String uuid) {
			super();
			this.binding = binding;
			this.astNode = astNode;
			this.key = key;
			this.uuid = uuid;
		}
		public IBinding binding;
		public ASTNode astNode;
		public String key;
		public String uuid;
	}

}