package edu.ustb.sei.mde.smartmerger.java;

public enum TagInfoKind {
	EXISTENCE,
	STRING,
	BOOLEAN,
	INTEGER,
	HASH
}