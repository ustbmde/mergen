package edu.ustb.sei.mde.smartmerger.java.visitors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

public class RenameInfo {

	public RenameInfo(ASTNode actualNode, String newName) {
		this.actualNode = actualNode;
		this.newName = newName;
		
		try {
			if(actualNode instanceof TypeDeclaration) {
				binding = ((TypeDeclaration) actualNode).resolveBinding();
			} else if(actualNode instanceof FieldDeclaration) {
				binding = ((VariableDeclarationFragment)(((FieldDeclaration) actualNode).fragments().get(0))).resolveBinding();
			} else if(actualNode instanceof MethodDeclaration) {
				binding = ((MethodDeclaration) actualNode).resolveBinding();
			}
		} catch (Exception e) {
			binding = null;
		}
		
		referencedPlaces = new ArrayList<>(16);
	}
	
	public ASTNode actualNode; // the node whose name is to be renamed
	public String newName;
	public IBinding binding; 
	public List<SimpleName> referencedPlaces;
}
