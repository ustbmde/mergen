package edu.ustb.sei.mde.smartmerger.java;

import java.util.List;
import java.util.Map;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.StructuralPropertyDescriptor;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import edu.ustb.sei.mde.smartmerger.java.visitors.CodeNormalizer;

public class TagInfoPattern {
	public static final String SMART_MERGER_LATEST = "_LATEST";
	
	public final String key;
	private String latestKey = null;
	
	
	public String getLatestedKey() {
		if(latestKey==null) latestKey = key+SMART_MERGER_LATEST;
		return latestKey;
	}
	
	public TagInfoPattern(String key, String tagName, TagInfoKind kind, String defaultStringLiteral) {
		super();
		this.key = tagName;
		this.tagName = tagName;
		this.kind = kind;
		this.defaultStringLiteral = defaultStringLiteral;
		this.hashDescriptorMap = Map.of();
	}
	
	public TagInfoPattern(String key, String tagName, TagInfoKind kind, String defaultStringLiteral, Map<Class<?>, StructuralPropertyDescriptor> map) {
		super();
		this.key = tagName;
		this.tagName = tagName;
		this.kind = kind;
		this.defaultStringLiteral = defaultStringLiteral;
		this.hashDescriptorMap = map;
	}

	public final String tagName;
	public final TagInfoKind kind;
	public final String defaultStringLiteral;
	public final Map<Class<?>, StructuralPropertyDescriptor> hashDescriptorMap;
	
	public void extracProperty(ASTNode node, TagElement tagElement) {
		String tagName = tagElement.getTagName();
		if(tagName==null) tagName = "";
		List<?> fragments = tagElement.fragments();
		
		if(this.tagName.equals(tagName)) {
			switch(kind) {
			case EXISTENCE: {
				node.setProperty(key, true);
			}
			break;
			case STRING: {
				String fragmentString = fragmentString(fragments);
				if(fragmentString!=null)
					node.setProperty(key, fragmentString);
			}
			break;
			case BOOLEAN: {
				String fragmentString = fragmentString(fragments);
				if(fragmentString!=null)
					node.setProperty(key, Boolean.parseBoolean(fragmentString));
			}
			break;
			case INTEGER: {
				try {
					String fragmentString = fragmentString(fragments);
					if(fragmentString!=null) {
						Long num = Long.parseLong(fragmentString);
						node.setProperty(key, num);						
					}
				} catch (NumberFormatException e) {
					node.setProperty(key, 0);
				}
			}
			break;
			case HASH: {
				String fragmentString = fragmentString(fragments);
				if(fragmentString!=null)
					node.setProperty(key, fragmentString);
			}
			break;
			}
			
		}
	}
	
	private String fragmentString(List<?> fragments) {
		if(fragments==null || fragments.isEmpty()) return defaultStringLiteral;
		if(fragments.size()==1) {
			return fragments.get(0).toString().trim();
		} else {
			StringBuilder builder = new StringBuilder();
			boolean first = true;
			for(Object f : fragments) {
				if(!first) builder.append(" ");
				else first = false;
				builder.append(f.toString());
			}
			return builder.toString().trim();
		}
	}
	
	protected String customizedHashCodeComputation(ASTNode node, CodeNormalizer normalizer) {
		return defaultStringLiteral;
	}
	
	@SuppressWarnings("rawtypes")
	final public String computeHashCode(ASTNode node, CodeNormalizer normalizer) {
		StructuralPropertyDescriptor desc = this.hashDescriptorMap.get(node.getClass());
		if(desc==null) {
			return customizedHashCodeComputation(node, normalizer);
		} else {
			try {
				Object prop = getNodeStructuralProperty(node, desc);

				if(prop!=null) {
					String literal = null;
					if(prop instanceof List) {
						StringBuilder builder = new StringBuilder();
						boolean isFirst = true;

						for(Object o : (List)prop) {
							if(isFirst) isFirst=false;
							else builder.append(",");
							((ASTNode)o).accept(normalizer);
							builder.append(normalizer.getAndReset());
						}
						literal = builder.toString();
					} else if(prop instanceof ASTNode) {
						if(isEntityName(desc)) {
							// for name, we do not turn it into uuid because we want to track it
							literal = ((Name)prop).getFullyQualifiedName();
						} else {
							((ASTNode)prop).accept(normalizer);
							literal = normalizer.getAndReset();							
						}
					} else {
						literal = prop.toString();
					}
					return JavaModelHelper.computeDigest(literal);
				}
				return defaultStringLiteral;
			} catch (Exception e) {
				e.printStackTrace();
				return defaultStringLiteral;
			}
		}
	}

	public Object getNodeStructuralProperty(ASTNode node, Class<?> clazz)  {
		if(clazz==null) clazz = node.getClass();
		StructuralPropertyDescriptor desc = this.hashDescriptorMap.get(clazz);
		if(desc==null) return null;
		return getNodeStructuralProperty(node, desc);
	}

	public Object getNodeStructuralProperty(ASTNode node, StructuralPropertyDescriptor desc) {
		Object prop;
		if(desc.getNodeClass()==VariableDeclarationFragment.class) {
			if(node instanceof FieldDeclaration) {						
				VariableDeclarationFragment fragment = (VariableDeclarationFragment) ((FieldDeclaration)node).fragments().get(0);
				prop = fragment.getStructuralProperty(desc);
			} else prop = null; // we do not know how to compute in this case
		} else {
			prop = node.getStructuralProperty(desc);
		}
		return prop;
	}
	
	static public boolean isJavadoctProperty(StructuralPropertyDescriptor nodeProperty) {
		return nodeProperty==TypeDeclaration.JAVADOC_PROPERTY || nodeProperty==FieldDeclaration.JAVADOC_PROPERTY || nodeProperty==MethodDeclaration.JAVADOC_PROPERTY
			 || nodeProperty==EnumDeclaration.JAVADOC_PROPERTY;
	}

	static public boolean isEntityName(StructuralPropertyDescriptor desc) {
		if(desc==VariableDeclarationFragment.NAME_PROPERTY
				|| desc==MethodDeclaration.NAME_PROPERTY
				|| desc==TypeDeclaration.NAME_PROPERTY) {
			return true;
		}
		return false;
	}
}