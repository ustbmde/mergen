package edu.ustb.sei.mde.smartmerger.java;

public enum ASTChangeKind {
	IDENTICAL, // all equal
	HASH_EQUAL,  // the case when source.hash=source.hash_latest=view.hash, but source.text!=view.text
	SOURCE_CHANGED, // the case when source.hash!=source.hash_latest && source.hash=view.hash
	VIEW_CHANGED, // the case when source.hash=source.hash_latest && source.hash!=view.hash
	BOTH_CHANGED, // the case when source.hash!=source.hash_latest && source.hash!=view.hash
	SOURCE_ABSENT, // the case when source is absent && view is not
	VIEW_ABSENT // the case when view is absent && source is not
}
