package edu.ustb.sei.mde.smartmerger.java;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTRequestor;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jface.text.IDocument;
import org.eclipse.text.edits.TextEdit;

import edu.ustb.sei.mde.smartmerger.java.visitors.ASTHashUpdater;
import edu.ustb.sei.mde.smartmerger.java.visitors.ASTHasher;
import edu.ustb.sei.mde.smartmerger.java.visitors.CodeNormalizer;
import edu.ustb.sei.mde.smartmerger.java.visitors.PropertyExtractor;

public class JavaModelHelper {
	static private MessageDigest digest = null;
	
	static public MessageDigest getDigest() {
		if(digest==null) {
			try {
				digest = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
		digest.reset();
		return digest;
	}
	
	static public String computeDigest(String text) {
		byte[] buf = getDigest().digest(text.getBytes());
		String md5Str = new BigInteger(1, buf).toString(16).toUpperCase();
		return md5Str;
	}
	
	static private IWorkspaceRoot workspaceRoot;
	static public IWorkspaceRoot getWorkspaceRoot() {
		if(workspaceRoot==null)
			workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		return workspaceRoot;
	}
	
	static private IJavaModel javaModel;
	static public IJavaModel getJavaModel() {
		if(javaModel==null)
			javaModel = JavaCore.create(getWorkspaceRoot());
		return javaModel;
	}
	
	static public List<ICompilationUnit> getAllCompilationUnits(IJavaProject project) {
		List<ICompilationUnit> units = new ArrayList<>(1024);
		try {
			getAllCompilationUnits(project, units);
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return units;
	}
	
	static private void getAllCompilationUnits(IJavaProject project, List<ICompilationUnit> units) throws JavaModelException {
		for(IPackageFragmentRoot root : project.getAllPackageFragmentRoots()) {
			getAllCompilationUnits(root, units);
		}
	}

	private static void getAllCompilationUnits(IPackageFragmentRoot root, List<ICompilationUnit> units)
			throws JavaModelException {
		if(root.isArchive() || root.isExternal() || root.isReadOnly())
			return;
		for(IJavaElement child : root.getChildren()) {
			IPackageFragment fragment = (IPackageFragment) child;
			for(ICompilationUnit unit : fragment.getCompilationUnits()) {
				units.add(unit);
			}
		}
	}
	
	static public Map<ICompilationUnit, CompilationUnit> collectASTs(IJavaProject project, List<ICompilationUnit> units) {
		ASTParser astParser = ASTParser.newParser(AST.getJLSLatest());
        astParser.setCompilerOptions(null);
        astParser.setResolveBindings(true);
        astParser.setProject(project);
        
        Map<ICompilationUnit, CompilationUnit> map = new HashMap<>();
        
        astParser.createASTs(units.toArray(new ICompilationUnit[units.size()]), new String[0], new ASTRequestor() {
        	@Override
        	public void acceptAST(ICompilationUnit source, CompilationUnit ast) {
        		map.put(source, ast);
        	}
		}, null);
        
        return map;
	}
	
	
	static public void extractASTInformation(TagPatternDictionary dict, Map<ICompilationUnit, CompilationUnit> maps, CodeIndexBase indexBase) {
		ASTVisitor propertyExtractor = new PropertyExtractor(dict, indexBase);
		
		maps.values().forEach(v->{
			v.accept(propertyExtractor);
		});
	}
	
	static public Map<ASTNode, ASTNode> reloadCodeBase(TagPatternDictionary dict, Map<ICompilationUnit, CompilationUnit> newMap, CodeBase codeBase) {
		Map<ASTNode, ASTNode> viewToNewViewMap = new HashMap<>();
		
		ASTVisitor propertyExtractor = new ASTVisitor() {
			@Override
			public boolean visit(FieldDeclaration node) {
				if(node.fragments().size()==1) {
					VariableDeclarationFragment singleFragment = (VariableDeclarationFragment) node.fragments().get(0);
					Javadoc doc = node.getJavadoc();
					int count = 0;
					for(Object o : doc.tags()) {
						TagElement tag = (TagElement) o;
						if(TagPatternDictionary.uuid.tagName.equals(tag.getTagName())) {
							TagPatternDictionary.uuid.extracProperty(node, tag);
							count++;
						} else if(TagPatternDictionary.isGenerated.tagName.equals(tag.getTagName())) {
							TagPatternDictionary.isGenerated.extracProperty(node, tag);
							count ++;
						}
						if(count==2) break;
					}
					
					@SuppressWarnings("unused")
					boolean isGenerted = node.getProperty(TagPatternDictionary.isGenerated.key) != null;
					String uuid = (String) node.getProperty(TagPatternDictionary.uuid.key);
					IVariableBinding binding = singleFragment.resolveBinding();
					
					ASTNode oldNode = null;
					
					if(uuid!=null) {
						oldNode = codeBase.indexBase.getFieldIndex().getASTNodeFromId(uuid);
					} else if(binding!=null){
						oldNode = codeBase.indexBase.getFieldIndex().getASTNodeFromKey(binding.getKey());
					}
					
					if(oldNode!=null)
						viewToNewViewMap.put(oldNode, node);
				}
				return false;
			}
			
			@Override
			public boolean visit(MethodDeclaration node) {
				Javadoc doc = node.getJavadoc();
				int count = 0;
				for(Object o : doc.tags()) {
					TagElement tag = (TagElement) o;
					if(TagPatternDictionary.uuid.tagName.equals(tag.getTagName())) {
						TagPatternDictionary.uuid.extracProperty(node, tag);
						count++;
					} else if(TagPatternDictionary.isGenerated.tagName.equals(tag.getTagName())) {
						TagPatternDictionary.isGenerated.extracProperty(node, tag);
						count ++;
					}
					if(count==2) break;
				}
				
				@SuppressWarnings("unused")
				boolean isGenerted = node.getProperty(TagPatternDictionary.isGenerated.key) != null;
				String uuid = (String) node.getProperty(TagPatternDictionary.uuid.key);
				IMethodBinding binding = node.resolveBinding();
				
				ASTNode oldNode = null;
				
				if(uuid!=null) {
					oldNode = codeBase.indexBase.getMethodIndex().getASTNodeFromId(uuid);
				} else if(binding!=null){
					oldNode = codeBase.indexBase.getMethodIndex().getASTNodeFromKey(binding.getKey());
				}
				
				if(oldNode!=null)
					viewToNewViewMap.put(oldNode, node);
				
				return false;
			}
			
			@Override
			public boolean visit(TypeDeclaration node) {
				Javadoc doc = node.getJavadoc();
				int count = 0;
				for(Object o : doc.tags()) {
					TagElement tag = (TagElement) o;
					if(TagPatternDictionary.uuid.tagName.equals(tag.getTagName())) {
						TagPatternDictionary.uuid.extracProperty(node, tag);
						count++;
					} else if(TagPatternDictionary.isGenerated.tagName.equals(tag.getTagName())) {
						TagPatternDictionary.isGenerated.extracProperty(node, tag);
						count ++;
					}
					if(count==2) break;
				}
				
				@SuppressWarnings("unused")
				boolean isGenerted = node.getProperty(TagPatternDictionary.isGenerated.key) != null;
				String uuid = (String) node.getProperty(TagPatternDictionary.uuid.key);
				ITypeBinding binding = node.resolveBinding();
				
				ASTNode oldNode = null;
				
				if(uuid!=null) {
					oldNode = codeBase.indexBase.getMethodIndex().getASTNodeFromId(uuid);
				} else if(binding!=null){
					oldNode = codeBase.indexBase.getMethodIndex().getASTNodeFromKey(binding.getKey());
				}
				
				if(oldNode!=null)
					viewToNewViewMap.put(oldNode, node);
				
				return true;
			}
		};
		
		newMap.values().forEach(v->{
			v.accept(propertyExtractor);
		});
		
		return viewToNewViewMap;
	}
	
	static public void printNormalizedAST(Map<ICompilationUnit, CompilationUnit> maps, CodeIndexBase indexBase) {
		ASTVisitor normalizer = new CodeNormalizer(indexBase) {
			@SuppressWarnings("restriction")
			public void endVisit(CompilationUnit node) {
				System.out.println(this.getResult());
				this.reset();
			};
		};
		
		maps.values().forEach(v->{
			v.accept(normalizer);
		});
		
	}
	
	static public void hashASTs(TagPatternDictionary dict, Map<ICompilationUnit, CompilationUnit> maps, CodeIndexBase indexBase) {
		CodeNormalizer normalizer = new CodeNormalizer(indexBase);
		ASTVisitor hasher = new ASTHasher(dict, normalizer);
		
		maps.values().forEach(v->{
			v.accept(hasher);
		});
	}
	
	static public void updateASTHashValues(TagPatternDictionary dict, Map<ICompilationUnit, CompilationUnit> maps, CodeIndexBase indexBase) {
		CodeNormalizer normalizer = new CodeNormalizer(indexBase);
		ASTHashUpdater hasher = new ASTHashUpdater(dict, normalizer);
		maps.entrySet().forEach(e->{
			e.getValue().accept(hasher);
			TextEdit edit = hasher.getTextEdit();
			if(edit!=null) {
				try {
					ICompilationUnit workingCopy = e.getKey().getWorkingCopy(null);
					workingCopy.applyTextEdit(edit, null);
					workingCopy.reconcile(ICompilationUnit.NO_AST, false, null, null);
					workingCopy.commitWorkingCopy(true, null);
					workingCopy.discardWorkingCopy();
				} catch (JavaModelException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
}
