package edu.ustb.sei.mde.smartmerger.java.visitors;

import java.util.List;

import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TextElement;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;
import org.eclipse.text.edits.TextEdit;

import edu.ustb.sei.mde.smartmerger.java.TagPatternDictionary;

public class ASTHashUpdater extends ASTHasher {
	private ASTRewrite rewriter;
	private CompilationUnit currentCU;
	
	@Override
	public boolean visit(CompilationUnit node) {
		rewriter = null;
		currentCU = node;
		return super.visit(node);
	}

	protected ASTRewrite getRewriter() {
		if(rewriter==null) {
			rewriter = ASTRewrite.create(currentCU.getAST());
		}
		return rewriter;
	}
	
	public TextEdit getTextEdit() {
		if(rewriter==null) return null;
		else {
			try {
				
				TextEdit edit = rewriter.rewriteAST();
				if(edit.getChildrenSize()>0 || edit.getLength()>0) return edit;
				else return null;
			} catch (JavaModelException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
	
	@Override
	public void endVisit(CompilationUnit node) {
		super.endVisit(node);
	}

	public ASTHashUpdater(TagPatternDictionary dict, CodeNormalizer normalizer) {
		super(dict, normalizer);
	}
	
	@Override
	protected void doUpdateHash(BodyDeclaration node, String key, String latestedKey, String tagName) {
		String hashValue = (String) node.getProperty(key);
		String latestHashValue = (String) node.getProperty(latestedKey);
		
		if(latestHashValue==null) {
			removeJavaDoc(node, tagName);
		} else if(hashValue==null || !hashValue.equals(latestHashValue)){
			updateJavaDoc(node, tagName, latestHashValue);
		}
	}
	
	protected void removeJavaDoc(BodyDeclaration node, String tagName) {
		Javadoc doc = node.getJavadoc();
		
		@SuppressWarnings("unchecked")
		List<TagElement> tagList = doc.tags();
		for(TagElement tag : tagList) {
			if(tagName.equals(tag.getTagName())) {
				getRewriter().remove(tag, null);
			}
		}
	}
	@SuppressWarnings("unchecked")
	protected void updateJavaDoc(BodyDeclaration node, String tagName, String value) {
		Javadoc doc = node.getJavadoc();
		
		boolean replaced = false;
		List<TagElement> tagList = doc.tags();
		for(TagElement tag : tagList) {
			if(tagName.equals(tag.getTagName())) {
				if(replaced==false) {
					TagElement newTag = buildTagElement(node.getAST(), tagName, value);
					getRewriter().replace(tag, newTag, null);
					replaced = true;				
				} else {
					getRewriter().remove(tag, null);
				}
			}
		}
		
		if(!replaced) {
			TagElement newTag = buildTagElement(node.getAST(), tagName, value);
			ListRewrite lwt = getRewriter().getListRewrite(doc, Javadoc.TAGS_PROPERTY);
			lwt.insertLast(newTag, null);
		}
	}

	@SuppressWarnings("unchecked")
	private TagElement buildTagElement(AST ast, String tagName, String value) {
		TagElement newTag = ast.newTagElement();
		newTag.setTagName(tagName);
		TextElement newTextElement = ast.newTextElement();
		newTextElement.setText(value);
		newTag.fragments().add(newTextElement);
		return newTag;
	}
}
