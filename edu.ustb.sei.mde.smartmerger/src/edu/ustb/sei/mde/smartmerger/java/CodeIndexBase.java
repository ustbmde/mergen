package edu.ustb.sei.mde.smartmerger.java;

import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class CodeIndexBase {
	private CodeIndex typeIndex = new CodeIndex();
	private CodeIndex fieldIndex = new CodeIndex();
	private CodeIndex methodIndex = new CodeIndex();
	
	public CodeIndex getTypeIndex() {
		return typeIndex;
	}
	
	public CodeIndex getFieldIndex() {
		return fieldIndex;
	}
	
	public CodeIndex getMethodIndex() {
		return methodIndex;
	}

	public void addIndex(ITypeBinding binding, AbstractTypeDeclaration node, String uuid) {
		typeIndex.addIndex(binding, node, uuid);
	}
	
	public void addIndex(IVariableBinding binding, FieldDeclaration node, String uuid) {
		fieldIndex.addIndex(binding, node, uuid);
	}
	
	public void addIndex(IMethodBinding binding, MethodDeclaration node, String uuid) {
		methodIndex.addIndex(binding, node, uuid);
	}

	public String getUUID(IBinding binding) {
		if(binding==null) return null;
		String key = binding.getKey();
		if(binding instanceof ITypeBinding) {
			return this.typeIndex.getIDFromKey(key);
		} else if(binding instanceof IVariableBinding) {
			return this.fieldIndex.getIDFromKey(key);
		} else if(binding instanceof IMethodBinding) {
			return this.methodIndex.getIDFromKey(key);
		}
		return null;
	}
	
}
