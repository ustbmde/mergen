package edu.ustb.sei.mde.smartmerger.java.visitors;

import java.util.Collection;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Javadoc;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TagElement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import edu.ustb.sei.mde.smartmerger.java.CodeIndexBase;
import edu.ustb.sei.mde.smartmerger.java.TagInfoPattern;
import edu.ustb.sei.mde.smartmerger.java.TagPatternDictionary;

public class PropertyExtractor extends ASTVisitor {
	private final TagPatternDictionary dict;
	private final CodeIndexBase indexBase;

	public PropertyExtractor(TagPatternDictionary dict, CodeIndexBase indexBase) {
		this.dict = dict;
		this.indexBase = indexBase;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean visit(EnumDeclaration node) {
		Collection<TagInfoPattern> patterns = dict.getPatterns(EnumDeclaration.class);
		Javadoc doc = node.getJavadoc();
		if(doc==null) return true;
		
		doc.tags().forEach(o->{
			TagElement tag = (TagElement)o;
			patterns.forEach(pat->pat.extracProperty(node, tag));
		});
		
		String uuid = (String) node.getProperty(TagPatternDictionary.uuid.key);
		ITypeBinding binding = node.resolveBinding();
		indexBase.addIndex(binding, node, uuid);
		
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean visit(TypeDeclaration node) {
		Collection<TagInfoPattern> patterns = dict.getPatterns(TypeDeclaration.class);
		Javadoc doc = node.getJavadoc();
		if(doc==null) return true;
		
		doc.tags().forEach(o->{
			TagElement tag = (TagElement)o;
			patterns.forEach(pat->pat.extracProperty(node, tag));
		});
		
		String uuid = (String) node.getProperty(TagPatternDictionary.uuid.key);
		ITypeBinding binding = node.resolveBinding();
		indexBase.addIndex(binding, node, uuid);
		
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean visit(FieldDeclaration node) {
		if(node.fragments().size()==1) {
			VariableDeclarationFragment singleFragment = (VariableDeclarationFragment) node.fragments().get(0);
			Collection<TagInfoPattern> patterns = dict.getPatterns(FieldDeclaration.class);
			Javadoc doc = node.getJavadoc();
			doc.tags().forEach(o->{
				TagElement tag = (TagElement)o;
				patterns.forEach(pat->pat.extracProperty(node, tag));
			});
			
			String uuid = (String) node.getProperty(TagPatternDictionary.uuid.key);
			IVariableBinding binding = singleFragment.resolveBinding();
			indexBase.addIndex(binding, node, uuid);
		}
		
		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean visit(MethodDeclaration node) {
		Collection<TagInfoPattern> patterns = dict.getPatterns(MethodDeclaration.class);
		Javadoc doc = node.getJavadoc();
		doc.tags().forEach(o->{
			TagElement tag = (TagElement)o;
			patterns.forEach(pat->pat.extracProperty(node, tag));
		});
		
		String uuid = (String) node.getProperty(TagPatternDictionary.uuid.key);
		IMethodBinding binding = node.resolveBinding();
		indexBase.addIndex(binding, node, uuid);
		
		return false;
	}
}