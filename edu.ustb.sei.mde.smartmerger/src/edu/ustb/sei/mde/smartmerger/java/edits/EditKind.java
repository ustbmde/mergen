package edu.ustb.sei.mde.smartmerger.java.edits;

public enum EditKind {
	KEEP, // this action is used due to the co-changes of source and view
	PUSH, // from source to view, for name (signature) only. Other properties will not cause any changes.
	PUSH_ALL, // used for child list
	PULL, // from view to source
	PULL_ALL, // used for child list
	REMOVE, // remove the source node
}
