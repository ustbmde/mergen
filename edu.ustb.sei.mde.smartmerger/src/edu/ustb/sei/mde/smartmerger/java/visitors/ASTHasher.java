package edu.ustb.sei.mde.smartmerger.java.visitors;

import java.util.stream.Stream;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import edu.ustb.sei.mde.smartmerger.java.TagInfoPattern;
import edu.ustb.sei.mde.smartmerger.java.TagPatternDictionary;

public class ASTHasher extends ASTVisitor {
	private final TagPatternDictionary dict;
	private final CodeNormalizer normalizer;
	private boolean commitHash;

	public ASTHasher(TagPatternDictionary dict, CodeNormalizer normalizer) {
		this(dict,normalizer,false);
	}
	
	public ASTHasher(TagPatternDictionary dict, CodeNormalizer normalizer, boolean commit) {
		this.dict = dict;
		this.normalizer = normalizer;
		this.commitHash = commit;
	}
	
	protected void computeHash(Class<?> explicitType, ASTNode node) {
		if(node.getProperty(TagPatternDictionary.isGenerated.key)!=null) {
			Stream<TagInfoPattern> patterns = dict.getHashPatterns(explicitType);
			patterns.forEach(p->{
				String hash = p.computeHashCode(node,normalizer);
				if(commitHash)
					node.setProperty(p.key, hash);
				else {
					node.setProperty(p.getLatestedKey(), hash);
					if(node instanceof BodyDeclaration)
						doUpdateHash((BodyDeclaration)node, p.key, p.getLatestedKey(), p.tagName);
				}
			});
		}
	}

	protected void doUpdateHash(BodyDeclaration node, String key, String latestedKey, String tag) {}

	@Override
	public boolean visit(TypeDeclaration node) {
		computeHash(TypeDeclaration.class, node);
		return true;
	}

	public boolean visit(FieldDeclaration node) {
		computeHash(FieldDeclaration.class, node);
		return false;
	}

	public boolean visit(MethodDeclaration node) {
		computeHash(MethodDeclaration.class, node);
		return false;
	}
}