package edu.ustb.sei.mde.smartmerger.java.visitors;

import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.internal.corext.dom.ASTFlattener;

import edu.ustb.sei.mde.smartmerger.java.CodeIndexBase;

@SuppressWarnings("restriction")
public class CodeNormalizer extends ASTFlattener {
	public String getAndReset() {
		String str = this.getResult();
		this.reset();
		return str;
	}
	
	public CodeNormalizer(CodeIndexBase base) {
		super();
		this.base = base;
	}

	protected CodeIndexBase base;
	
	@Override
	public boolean visit(QualifiedName node) {
		IBinding binding = node.resolveBinding();
		String uuid = base.getUUID(binding);
		
		if(uuid==null) {
			return super.visit(node);
		} else {
			this.fBuffer.append(uuid);
			return false;
		}
	}
	
	@Override
	public boolean visit(SimpleName node) {
		if(node.getParent() instanceof QualifiedName) {
			return super.visit(node);
		} else {
			IBinding binding = node.resolveBinding();
			String uuid = base.getUUID(binding);
			if(uuid==null) {
				return super.visit(node);
			} else {
				this.fBuffer.append(uuid);
				return false;
			}
		}
	}
	
//	@Override
//	public boolean visit(MethodDeclaration node) {
//		// TODO Auto-generated method stub
//		return super.visit(node);
//	}
	
}
