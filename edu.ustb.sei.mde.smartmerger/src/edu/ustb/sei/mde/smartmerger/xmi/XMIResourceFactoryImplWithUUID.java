package edu.ustb.sei.mde.smartmerger.xmi;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class XMIResourceFactoryImplWithUUID extends XMIResourceFactoryImpl {

	public XMIResourceFactoryImplWithUUID() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Resource createResource(URI uri) {
		return new XMIResourceImplWithUUID(uri);
	}

}
