package edu.ustb.sei.mde.smartmerger.xmi;

import org.eclipse.emf.codegen.ecore.genmodel.GenBase;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;

public class CodeGenHelper {

	static public String genUUID(GenBase base, String prefix, boolean isImpl) {
		EModelElement element = base.getEcoreModelElement();
		Resource eResource = element.eResource();
		
		if(eResource instanceof XMLResource) {
			String id = ((XMLResource) eResource).getID(element);
			if(id==null) {
				id = EcoreUtil.generateUUID();
				((XMLResource) eResource).setID(element, id);
			}
			return "@uuid "+prefix + (isImpl?"-IMPL-":"-DECL-") + id;
		} else 
			return "";
	}
	static public String genUUID(GenBase base, GenBase base2, String prefix, boolean isImpl) {
		EModelElement element = base.getEcoreModelElement();
		Resource eResource = element.eResource();
		EModelElement element2 = base2.getEcoreModelElement();
		Resource eResource2 = element2.eResource();
		
		if(eResource instanceof XMLResource) {
			String id = ((XMLResource) eResource).getID(element);
			if(id==null) {
				id = EcoreUtil.generateUUID();
				((XMLResource) eResource).setID(element, id);
			}
			String id2 = ((XMLResource) eResource2).getID(element2);
			if(id2==null) {
				id2 = EcoreUtil.generateUUID();
				((XMLResource) eResource2).setID(element2, id2);
			}
			return "@uuid "+prefix + (isImpl?"-IMPL-":"-DECL-")+id+'-'+id2;
		} else 
			return "";
	}
	
	static public String genUUID(GenBase base, GenBase base2, String prefix) {
		EModelElement element = base.getEcoreModelElement();
		Resource eResource = element.eResource();
		EModelElement element2 = base2.getEcoreModelElement();
		
		if(eResource instanceof XMLResource) {
			String id = ((XMLResource) eResource).getID(element);
			if(id==null) {
				id = EcoreUtil.generateUUID();
				((XMLResource) eResource).setID(element, id);
			}
			String id2 = ((XMLResource) eResource).getID(element2);
			if(id2==null) {
				id2 = EcoreUtil.generateUUID();
				((XMLResource) eResource).setID(element2, id2);
			}
			return "@uuid "+prefix+"-"+id+'-'+id2;
		} else 
			return "";
	}
}
