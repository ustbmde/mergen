package edu.ustb.sei.mde.smartmerger.ui;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;

import edu.ustb.sei.mde.smartmerger.java.CodeIndexBase;
import edu.ustb.sei.mde.smartmerger.java.JavaModelHelper;
import edu.ustb.sei.mde.smartmerger.java.TagPatternDictionary;
import edu.ustb.sei.mde.smartmerger.xmi.XMIResourceImplWithUUID;

public class GenUUID extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelectionService selectionService = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
		ISelection selection = selectionService.getSelection();
		
		if(selection!=null) {
			if(selection instanceof IStructuredSelection) {
				Object first = ((IStructuredSelection) selection).getFirstElement();
				if(first instanceof IFile) {
					IFile file = (IFile) first;
					String path = file.getFullPath().toString();
					if(path.endsWith(".ecore")) {
						ResourceSet set = new ResourceSetImpl();
						set.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
								Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl() {
									@Override
									public Resource createResource(URI uri) {
										return new XMIResourceImplWithUUID(uri);
									}
								});
						Resource res = set.getResource(URI.createPlatformResourceURI(path, true), true);
						res.setModified(true);
						try {
							res.save(null);
						} catch (IOException e) {
							e.printStackTrace();
						};
					}
				}
			}
		}
		return null;
	}

}
