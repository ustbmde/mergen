package edu.ustb.sei.mde.smartmerger.ui;

import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;

import edu.ustb.sei.mde.smartmerger.java.CodeIndexBase;
import edu.ustb.sei.mde.smartmerger.java.JavaModelHelper;
import edu.ustb.sei.mde.smartmerger.java.TagPatternDictionary;

public class IndexHandler extends AbstractHandler implements IHandler {

	public IndexHandler() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ISelectionService selectionService = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
		
		
		ISelection selection = selectionService.getSelection();
		
		if(selection!=null) {
			if(selection instanceof IStructuredSelection) {
				Object first = ((IStructuredSelection) selection).getFirstElement();
				if(first instanceof IProject) {
					IJavaProject javaProject = JavaModelHelper.getJavaModel().getJavaProject(((IProject)first).getName());
					if(javaProject!=null && javaProject.exists()) {
						List<ICompilationUnit> cus = JavaModelHelper.getAllCompilationUnits(javaProject);
						Map<ICompilationUnit, CompilationUnit> astMaps = JavaModelHelper.collectASTs(javaProject, cus);
						TagPatternDictionary dict = new TagPatternDictionary();
						CodeIndexBase base = new CodeIndexBase();
						JavaModelHelper.extractASTInformation(dict, astMaps, base);
						
						JavaModelHelper.printNormalizedAST(astMaps, base);
					}
				}
			}
		}
		return null;
	}

}
