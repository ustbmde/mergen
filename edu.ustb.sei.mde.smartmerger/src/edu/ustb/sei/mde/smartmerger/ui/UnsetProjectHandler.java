package edu.ustb.sei.mde.smartmerger.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;

import edu.ustb.sei.mde.smartmerger.Activator;

public class UnsetProjectHandler extends AbstractHandler implements IHandler {

	public UnsetProjectHandler() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String target = event.getParameter("edu.ustb.sei.mde.smartmerger.project");
		if(target.equals("source"))
			Activator.getDefault().sourceProject = null;
		else Activator.getDefault().viewProject = null;
		
		return null;
	}

}
