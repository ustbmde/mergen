package edu.ustb.sei.mde.smartmerger.ui;

import java.util.Arrays;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;

import edu.ustb.sei.mde.smartmerger.Activator;
import edu.ustb.sei.mde.smartmerger.java.CodeBase;
import edu.ustb.sei.mde.smartmerger.java.TagPatternDictionary;
import edu.ustb.sei.mde.smartmerger.java.visitors.ASTMerger;

public class MergeHandler extends AbstractHandler implements IHandler {

	public MergeHandler() {}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IJavaProject sourceProject = Activator.getDefault().sourceProject;
		IJavaProject viewProject = Activator.getDefault().viewProject;
		
		TagPatternDictionary dict = new TagPatternDictionary();
		
		CodeBase sourceBase = CodeBase.build(sourceProject, dict);
		CodeBase viewBase = CodeBase.build(viewProject, dict);
		
		ASTMerger merger = new ASTMerger(dict, sourceBase, viewBase);
		merger.doMatch();
		return null;
	}


	@Override
	public boolean isEnabled() {
		IJavaProject sourceProject = Activator.getDefault().sourceProject;
		IJavaProject viewProject = Activator.getDefault().viewProject;
		
		return (sourceProject!=null && viewProject!=null && sourceProject!=viewProject);
	}

}
