package edu.ustb.sei.mde.smartmerger.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;

import edu.ustb.sei.mde.smartmerger.Activator;
import edu.ustb.sei.mde.smartmerger.java.JavaModelHelper;

public class SetProjectHandler extends AbstractHandler implements IHandler {

	public SetProjectHandler() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String target = event.getParameter("edu.ustb.sei.mde.smartmerger.project");
		
		ISelectionService selectionService = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
		
		
		ISelection selection = selectionService.getSelection();
		
		if(selection!=null) {
			if(selection instanceof IStructuredSelection) {
				Object first = ((IStructuredSelection) selection).getFirstElement();
				if(first instanceof IProject) {
					IJavaProject javaProject = JavaModelHelper.getJavaModel().getJavaProject(((IProject)first).getName());
					if(target.equals("source"))
						Activator.getDefault().sourceProject = javaProject;
					else Activator.getDefault().viewProject = javaProject;
				}
			}
		}
		return null;
	}

}
